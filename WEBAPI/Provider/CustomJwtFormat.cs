﻿using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using WebAPI.Core.Code;

namespace WEBAPI.Provider
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private IGlobal _global;
        #region CTOR
        public CustomJwtFormat(IGlobal global)
        {
            _global = global;
        }
        #endregion

        public string Protect(AuthenticationTicket data)
        {
            if (data == null || data.Identity.Claims == null || data.Identity.Claims.Count() == 0)
            {
                throw new ArgumentNullException("data");
            }
            string audienceId = _global.AudienceID();
            string _issuer = _global.Issuer();
            string symmetricKeyAsBase64 = _global.SignKey();

            var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);

            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(keyByteArray);
            var signingKey = new Microsoft.IdentityModel.Tokens.SigningCredentials(
                        securityKey, SecurityAlgorithms.HmacSha256);

            var issued = data.Properties.IssuedUtc;

            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityToken(_issuer, audienceId, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey);

            var handler = new JwtSecurityTokenHandler();

            var jwt = handler.WriteToken(token);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            return null;
        }
    }
}