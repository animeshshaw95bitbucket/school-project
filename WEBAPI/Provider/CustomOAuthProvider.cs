﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using WebAPI.Core.Code;
using WebAPI.Data.Code;

namespace WEBAPI.Provider
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        private IRole _role;
        private IAuthenticate _authenticate;
        private IGlobal _global;
        #region CTOR
        public CustomOAuthProvider(IGlobal global)
        {
            _role = new RoleBusiness(global);
            _authenticate = new AuthenticateBusiness(global);
            _global = global;
        }
        #endregion
        string clientId = string.Empty;
        private async Task<List<Claim>> ReadDataAsync(string ipAddress, string Username, string Password)
        {
            List<Claim> claims = new List<Claim>();
            try
            {
                var userDetails = _authenticate.UserLogin(new WebAPI.Core.RequestModel.AuthenticateRequestModel() { UserName = Username, Password = Password });
                if (userDetails != null && userDetails.UserID > 0)
                {
                    claims.Add(new Claim("UserID", userDetails.UserID.ToString()));
                    claims.Add(new Claim("UserDetails", JsonConvert.SerializeObject(userDetails)));
                    claims.Add(new Claim(ClaimTypes.Name, userDetails.FirstName+" "+userDetails.LastName));
                    var roles = _role.GetUserRoles(userDetails.UserID);
                    if (roles != null)
                    {
                        foreach (var role in roles)
                            claims.Add(new Claim(ClaimTypes.Role, role.RoleName));
                    }
                }
            }
            catch (Exception ex) { }
            return claims;
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            string ipAddress = context.Request.RemoteIpAddress;
            List<Claim> claimList = await ReadDataAsync(ipAddress, context.UserName, context.Password);

            if (claimList == null || (claimList != null && claimList.Count() == 0))
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }


            ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claimList);
            var ticket = new AuthenticationTicket(oAuthIdentity, null);

            context.Validated(ticket);
        }
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientSecret = string.Empty;
            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {

            }
            context.Validated();
        }
    }
}