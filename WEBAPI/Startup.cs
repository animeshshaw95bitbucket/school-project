﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(WEBAPI.Startup))]

namespace WEBAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            //Web API routes
            config.MapHttpAttributeRoutes();
            ConfigureAuth(app);
        }
    }
}
