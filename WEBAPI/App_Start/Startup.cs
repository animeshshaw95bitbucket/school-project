﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.OAuth;
using WEBAPI.Provider;
using WebAPI.Core.Code;
using WebAPI.Code;
using WebAPI.Data.Code;

[assembly: OwinStartup(typeof(WEBAPI.Startup))]

namespace WEBAPI
{
    public partial class Startup
    {
        IGlobal _global = GetGlobal();
        public void ConfigureAuth(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            HttpConfiguration config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            double accessTokenExpireTimeSpan = 30; // Default value 30 minutes
            Double.TryParse(_global.AuthTokenExpiry(), out accessTokenExpireTimeSpan);
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(accessTokenExpireTimeSpan),
                Provider = new CustomOAuthProvider(_global),
                AccessTokenFormat = new CustomJwtFormat(_global),

            };
            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
        }
        private static IGlobal GetGlobal()
        {
            String DBConn = GlobalConstants.WebAPIPlatformDBConn();
            Int32 DefaultPageListSize = GlobalConstants.DefaultPageListSize;
            String WebsiteName = GlobalConstants.WebsiteName;
            String WebsiteURL = GlobalConstants.WebsiteURL;
            String AuthTokenExpiry = GlobalConstants.AuthTokenExpiry;
            String FileUploadPath = GlobalConstants.FileUploadPath;
            String FrontEndWebsiteURL = GlobalConstants.FrontEndWebsiteURL;
            String FileUploadRawPath = GlobalConstants.FileUploadRawPath;
            String IOSCert = GlobalConstants.IOSCert;
            String Error = GlobalConstants.Error;
            String TempFolderPath = GlobalConstants.TempFolderPath;
            String FrontEndWebFolderPath = GlobalConstants.FrontEndWebFolderPath;
            String GeneralErrorMessgae = GlobalConstants.GeneralErrorMessgae;
            String audienceID = GlobalConstants.audienceID;
            String issuer = GlobalConstants.issuer;
            String signKey = GlobalConstants.signKey;

            Global global = new Global(DBConn, DefaultPageListSize, WebsiteName, WebsiteURL, AuthTokenExpiry, FileUploadPath, FrontEndWebFolderPath, FileUploadRawPath, IOSCert, Error, TempFolderPath, FrontEndWebFolderPath, GeneralErrorMessgae, audienceID, issuer, signKey);
            return global;
        }
    }
}
