﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEBAPI.App_Start
{
    using Ninject;
    using Ninject.Extensions.ChildKernel;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Http.Dependencies;
    using WebAPI.Code;
    using WebAPI.Core.Code;
    using WebAPI.Data.Code;

    namespace WEBAPI.App_Start
    {
        public class NinjectResolver : IDependencyResolver
        {
            private IKernel kernel;

            public NinjectResolver() : this(new StandardKernel())
            {
            }

            public NinjectResolver(IKernel ninjectKernel, bool scope = false)
            {
                kernel = ninjectKernel;
                if (!scope)
                {
                    AddBindings(kernel);
                }
            }

            public IDependencyScope BeginScope()
            {
                return new NinjectResolver(AddRequestBindings(new ChildKernel(kernel)), true);
            }

            public object GetService(Type serviceType)
            {
                return kernel.TryGet(serviceType);
            }

            public IEnumerable<object> GetServices(Type serviceType)
            {
                return kernel.GetAll(serviceType);
            }

            public void Dispose()
            {

            }

            private void AddBindings(IKernel kernel)
            {
                // singleton and transient bindings go here
            }

            private IKernel AddRequestBindings(IKernel kernel)
            {
                String DBConn = GlobalConstants.WebAPIPlatformDBConn();
                Int32 DefaultPageListSize = GlobalConstants.DefaultPageListSize;
                String WebsiteName = GlobalConstants.WebsiteName;
                String WebsiteURL = GlobalConstants.WebsiteURL;
                String AuthTokenExpiry = GlobalConstants.AuthTokenExpiry;
                String FileUploadPath = GlobalConstants.FileUploadPath;
                String FrontEndWebsiteURL = GlobalConstants.FrontEndWebsiteURL;
                String FileUploadRawPath = GlobalConstants.FileUploadRawPath;
                String IOSCert = GlobalConstants.IOSCert;
                String Error = GlobalConstants.Error;
                String TempFolderPath = GlobalConstants.TempFolderPath;
                String FrontEndWebFolderPath = GlobalConstants.FrontEndWebFolderPath;
                String GeneralErrorMessgae = GlobalConstants.GeneralErrorMessgae;
                String audienceID = GlobalConstants.audienceID;
                String issuer = GlobalConstants.issuer;
                String signKey = GlobalConstants.signKey;


                kernel.Bind<IGlobal>()
                            .To<Global>()
                            .InSingletonScope()
                            .WithConstructorArgument("connectionstring", DBConn)
                            .WithConstructorArgument("defaultpagelistsize", DefaultPageListSize)
                            .WithConstructorArgument("websitename", WebsiteName)
                            .WithConstructorArgument("websiteurl", WebsiteURL)
                            .WithConstructorArgument("AuthTokenExpiry", AuthTokenExpiry)
                            .WithConstructorArgument("fileuploadpath", FileUploadPath)
                            .WithConstructorArgument("frontendwebsiteurl", FrontEndWebsiteURL)
                            .WithConstructorArgument("fileuploadrawpath", FileUploadRawPath)
                            .WithConstructorArgument("IOSCert", IOSCert)
                            .WithConstructorArgument("error", Error)
                            .WithConstructorArgument("tempFolderPath", TempFolderPath)
                            .WithConstructorArgument("frontEndWebFolderPath", FrontEndWebFolderPath)
                            .WithConstructorArgument("generalErrorMessgae", GeneralErrorMessgae)
                            .WithConstructorArgument("audienceID", audienceID)
                            .WithConstructorArgument("issuer", issuer)
                            .WithConstructorArgument("signKey", signKey);

                kernel.Bind<IAuthenticate>().To<AuthenticateBusiness>().InSingletonScope();
                kernel.Bind<IHeadOfficeMaster>().To<HeadOfficeMasterBusiness>().InSingletonScope();
                kernel.Bind<IRole>().To<RoleBusiness>().InSingletonScope();
                kernel.Bind<IBranch>().To<BranchBusiness>().InSingletonScope();
                kernel.Bind<IUser>().To<UserBusiness>().InSingletonScope();
                kernel.Bind<IStudent>().To<StudentBusiness>().InSingletonScope();

                return kernel;
            }
        }
    }
}