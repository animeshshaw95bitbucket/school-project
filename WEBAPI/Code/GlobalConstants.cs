﻿using System;
using System.Configuration;

namespace WebAPI.Code
{
    /// <summary>
    /// 
    /// </summary>
    public static class GlobalConstants
    {
        // Public objects.       
        public static Int32 DefaultPageListSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageListSize"]);
        public static String WebsiteName = ConfigurationManager.AppSettings["WebsiteName"];
        public static String WebsiteURL = ConfigurationManager.AppSettings["WebsiteURL"];
        public static String AuthTokenExpiry = ConfigurationManager.AppSettings["AuthTokenExpiry"];
        public static String FileUploadPath = ConfigurationManager.AppSettings["FilePath"];
        public static String FrontEndWebsiteURL = ConfigurationManager.AppSettings["FrontEndWebsiteURL"];
        public static String FileUploadRawPath = ConfigurationManager.AppSettings["FileUploadRawPath"];
        public static String IOSCert = ConfigurationManager.AppSettings["IOSCert"];
        public static String Error = ConfigurationManager.AppSettings["Error"];
        public static String BlockchainConsoleAppPath = ConfigurationManager.AppSettings["BlockchainConsoleAppPath"];
        public static String TempFolderPath = ConfigurationManager.AppSettings["TempFolderPath"];
        public static String FrontEndWebFolderPath = ConfigurationManager.AppSettings["FrontEndWebFolderPath"];
        public static String audienceID = ConfigurationManager.AppSettings["audienceID"];
        public static String issuer = ConfigurationManager.AppSettings["issuer"];
        public static String signKey = ConfigurationManager.AppSettings["signKey"];

        // Data connection objects.
        private static String DBConn = ConfigurationManager.AppSettings["WebAPIPlatformDBConn"];

        public static String GeneralErrorMessgae = "An error occurred.";

        /// <summary>
        /// Data connections.
        /// </summary>
        /// <returns></returns>
        public static String WebAPIPlatformDBConn()
        {
            return ConfigurationManager.ConnectionStrings[DBConn].ConnectionString;
        }
    }
}
