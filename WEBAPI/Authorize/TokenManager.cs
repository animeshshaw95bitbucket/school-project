﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.ServiceModel.Security.Tokens;
using System.Text;
using System.Threading.Tasks;


namespace WEBAPI
{
    public class TokenManager
    {

        public TokenManager()
        {


        }

        public string CreateJwtToken(string signKey, List<Claim> claimList, double expirationMinutes, string issuer, string applyToAddress)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var credential = Convert.FromBase64String(signKey);
            //var sSKey = new InMemorySymmetricSecurityKey(credential);
            Microsoft.IdentityModel.Tokens.SecurityKey sSKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(credential);
            var now = DateTime.UtcNow;
            Claim[] claims = claimList.ToArray();
            Microsoft.IdentityModel.Tokens.SecurityTokenDescriptor sc = new Microsoft.IdentityModel.Tokens.SecurityTokenDescriptor()
            {

                Subject = new ClaimsIdentity(claims),
                Issuer = (!string.IsNullOrWhiteSpace(issuer)) ? issuer : string.Empty,
                Audience = (!string.IsNullOrWhiteSpace(applyToAddress) && (applyToAddress.LastIndexOf("http", StringComparison.OrdinalIgnoreCase) >= 0)) ? applyToAddress : string.Empty,
                Expires = now.AddMinutes(expirationMinutes),
                SigningCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(sSKey,
                    "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                    "http://www.w3.org/2001/04/xmlenc#sha256"),

                //TokenIssuerName = (!string.IsNullOrWhiteSpace(issuer)) ? issuer : string.Empty,

                //AppliesToAddress = (!string.IsNullOrWhiteSpace(applyToAddress) && (applyToAddress.LastIndexOf("http", StringComparison.OrdinalIgnoreCase) >= 0)) ? applyToAddress : string.Empty,
                //Lifetime = new Lifetime(now, now.AddMinutes(expirationMinutes)),
                //SigningCredentials = new SigningCredentials(sSKey,
                //    "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                //    "http://www.w3.org/2001/04/xmlenc#sha256"),
            };

            var jwtToken = tokenHandler.CreateToken(sc);

            return tokenHandler.WriteToken(jwtToken);
        }
        public ClaimsPrincipal ValidateJwtToken(string signKey, string jwtToken, string audienceID, string issuer)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var credential = Convert.FromBase64String(signKey);
            var sSKey = new InMemorySymmetricSecurityKey(credential);

            // Parse JWT from the Base64UrlEncoded wire form (<Base64UrlEncoded header>.<Base64UrlEncoded body>.<signature>)
            JwtSecurityToken parsedJwt = tokenHandler.ReadToken(jwtToken) as JwtSecurityToken;

            TokenValidationParameters validationParams =
                new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidAudience = (!string.IsNullOrWhiteSpace(audienceID)) ? audienceID.ToLower() : "",
                    ValidIssuer = (!string.IsNullOrWhiteSpace(issuer)) ? issuer.ToLower() : "", // TokenIssuer,
                    ValidateAudience = true,
                    ValidateIssuer = true,
                    IssuerSigningKey= new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(credential)
                    //IssuerSigningToken = new BinarySecretSecurityToken(credential),
                };
            try
            {
                Microsoft.IdentityModel.Tokens.SecurityToken validatedToken;
                var principal = tokenHandler.ValidateToken(jwtToken, validationParams, out validatedToken);
                return principal;
            }
            catch (Exception e)
            {

                Console.WriteLine("{0}\n {1}", e.Message, e.StackTrace);

            }
            return null;


        }

    }
}

