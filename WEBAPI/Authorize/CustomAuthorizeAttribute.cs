﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using WebAPI.Code;
using WebAPI.Core.Code;

namespace WEBAPI
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
       public override void OnAuthorization(HttpActionContext actionContext)
        {
            string sKey = GlobalConstants.signKey;
            string auId = GlobalConstants.audienceID;
            string issuer = GlobalConstants.issuer;

            var roles = this.Roles;
            var request = actionContext.Request;

            if (request.Headers.Authorization == null)
            {
                var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("{\"Message\": \"Authorization has been denied for this request.\" }", Encoding.UTF8, "application/json")
                };
                responseMessage.Headers.WwwAuthenticate.Add(
                                new AuthenticationHeaderValue("Bearer",
                                    "error=\"invalid_token\""));
                responseMessage.RequestMessage = request;
                actionContext.Response = responseMessage;
            }
            string jwtToken;
            if (!TryRetrieveToken(request, out jwtToken))
            {
                var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("{\"Message\": \"Authorization has been denied for this request.\" }", Encoding.UTF8, "application/json")
                };
                responseMessage.Headers.WwwAuthenticate.Add(
                                new AuthenticationHeaderValue("Bearer",
                                    "error=\"invalid_token\""));
                responseMessage.RequestMessage = request;
                actionContext.Response = responseMessage;
            }

            try
            {
                TokenManager tm = new TokenManager();
                var principal = tm.ValidateJwtToken(sKey, jwtToken, auId, issuer);
                bool isValidIP = false;
                if (principal != null)
                {

                    string remoteIP = GetClientIpAddress(request);
                    isValidIP =
                        principal.Claims.Any(
                            c =>
                                c.Type.Equals("validIps", StringComparison.OrdinalIgnoreCase) &&
                                (c.Value.Contains("*") || c.Value.Contains(remoteIP)));

                    if (!isValidIP)
                    {
                        var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                        {
                            Content = new StringContent("{\"Message\": \"Authorization has been denied for this request.\" }", Encoding.UTF8, "application/json")
                        };
                        responseMessage.Headers.WwwAuthenticate.Add(
                                        new AuthenticationHeaderValue("Bearer",
                                            "error=\"invalid_token\""));
                        responseMessage.RequestMessage = request;
                        actionContext.Response = responseMessage;
                    }
                    if (!string.IsNullOrEmpty(this.Roles))
                    {
                        string[] arrRoles = this.Roles.Split(',');
                        string UserRole = Convert.ToString(principal.Claims.Where(x => x.Type == "role").Select(y => y.Value).FirstOrDefault());
                        if (!arrRoles.Contains(UserRole))
                        {
                            var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                            {
                                Content = new StringContent("{\"Message\": \"Authorization has been denied for this request.\" }", Encoding.UTF8, "application/json")
                            };
                            responseMessage.Headers.WwwAuthenticate.Add(
                                            new AuthenticationHeaderValue("Bearer",
                                                "error=\"invalid_token\""));
                            responseMessage.RequestMessage = request;
                            actionContext.Response = responseMessage;
                        }
                    }
                }
                else
                {
                    var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        Content = new StringContent("{\"Message\": \"Authorization has been denied for this request.\" }", Encoding.UTF8, "application/json")
                    };
                    responseMessage.Headers.WwwAuthenticate.Add(
                                    new AuthenticationHeaderValue("Bearer",
                                        "error=\"invalid_token\""));
                    responseMessage.RequestMessage = request;
                    actionContext.Response = responseMessage;
                }
            }
            catch (SecurityTokenValidationException)
            {
                var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("{\"Message\": \"Authorization has been denied for this request.\" }", Encoding.UTF8, "application/json")
                };
                responseMessage.Headers.WwwAuthenticate.Add(
                                new AuthenticationHeaderValue("Bearer",
                                    "error=\"invalid_token\""));
                responseMessage.RequestMessage = request;
                actionContext.Response = responseMessage;
            }
            catch (Exception e)
            {
                var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("{\"Message\": \"Authorization has been denied for this request.\" }", Encoding.UTF8, "application/json")
                };
                responseMessage.Headers.WwwAuthenticate.Add(
                                new AuthenticationHeaderValue("Bearer",
                                    "error=\"invalid_token\""));
                responseMessage.RequestMessage = request;
                actionContext.Response = responseMessage;
            }


        }
        private string GetClientIpAddress(HttpRequestMessage request)
        {
            string HttpContext = "MS_HttpContext";
            string RemoteEndpointMessage = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";


            if (request.Properties.ContainsKey(HttpContext))
            {
                dynamic ctx = request.Properties[HttpContext];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }

            if (request.Properties.ContainsKey(RemoteEndpointMessage))
            {
                dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }

            return null;
        }
        // Reads the token from the authorization header on the incoming request
        private static bool TryRetrieveToken(HttpRequestMessage request, out string token)
        {
            token = null;

            if (!request.Headers.Contains("Authorization"))
            {
                return false;
            }

            string authzHeader = request.Headers.GetValues("Authorization").First<string>();

            // Verify Authorization header contains 'Bearer' scheme
            token = authzHeader.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase) ? authzHeader.Split(' ')[1] : null;

            if (null == token)
            {
                return false;
            }

            return true;
        }
    }
}
