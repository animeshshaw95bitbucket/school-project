﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Core.Code;
using WebAPI.Data.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using System.Web.Http.Cors;

namespace WEBAPI.Controllers
{
    //[CustomAuthorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    [RoutePrefix("api/v1/student")]
    public class StudentController : ApiController
    {
        IStudent _student;
        IGlobal _global;

        #region CTOR
        public StudentController()
        {
            _global = new Global();
            _student = new StudentBusiness(_global);
        }
        #endregion

        #region Student Query

        [Route("Query/AddUpdate")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage AddUpdateStudentQuery(AddUpdateStudentQueryRequest request)
        {
            CommonResponseDataModel resp = new CommonResponseDataModel();
            try
            {
                if (!ModelState.IsValid)
                {
                    resp.Code = 400;
                    resp.Message = JsonConvert.SerializeObject(ModelState);
                    return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.BadRequest, resp, "application/json");
                }
                resp = _student.AddUpdateStudentQuery(request);
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }

        [Route("Query/IsRead")]
        [HttpGet]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage UpdateIsReadStudentQuery(int StudentQueryID, bool IsRead)
        {
            CommonResponseDataModel resp = new CommonResponseDataModel();
            try
            {
                if (!ModelState.IsValid)
                {
                    resp.Code = 400;
                    resp.Message = JsonConvert.SerializeObject(ModelState);
                    return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.BadRequest, resp, "application/json");
                }
                resp = _student.UpdateIsReadStudentQuery(StudentQueryID, IsRead);
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }


        [Route("Query/all")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<StudentQueryResponseDataModel>))]
        public HttpResponseMessage GetStudentQuery([FromUri] SearchStudentQueryRequest request)
        {
            APIResponseDataModel<StudentQueryResponseDataModel> resp = new APIResponseDataModel<StudentQueryResponseDataModel>();
            try
            {
                var data = _student.GetStudentQuery(request);
                resp.Code = 200;
                resp.Message = "";
                resp.Data = data.ToList();
                return Request.CreateResponse<APIResponseDataModel<StudentQueryResponseDataModel>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<StudentQueryResponseDataModel>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }

        [Route("Query/Genders")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<GenderResponseDataModel>))]
        public HttpResponseMessage Genders()
        {
            var GenderData = new List<GenderResponseDataModel>();
            GenderData.Add(new GenderResponseDataModel { GenderID = 1, Gender = "Male" });
            GenderData.Add(new GenderResponseDataModel { GenderID = 2, Gender = "Female" });
            GenderData.Add(new GenderResponseDataModel { GenderID = 3, Gender = "Other" });

            APIResponseDataModel<GenderResponseDataModel> resp = new APIResponseDataModel<GenderResponseDataModel>();
            try
            {
                resp.Code = 200;
                resp.Message = "";
                resp.Data = GenderData;
                return Request.CreateResponse<APIResponseDataModel<GenderResponseDataModel>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<GenderResponseDataModel>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }
        #endregion
    }
}