﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Core.Code;
using WebAPI.Data.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using System.Web.Http.Cors;

namespace WEBAPI.Controllers
{
    //[CustomAuthorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    [RoutePrefix("api/v1/branch")]
    public class BranchController : ApiController
    {
        IBranch _branch;
        IGlobal _global;
        #region CTOR
        public BranchController()
        {
            _global = new Global();
            _branch = new BranchBusiness(_global);
        }
        #endregion
        [Route("getbranchdetails")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<BranchDetails>))]
        public HttpResponseMessage GetBranchDetails(String branchName = null, int? BranchID = null, int? BranchOwnerID = null, int? BranchTypeID = null,int? StateID=null,int? DistrictID=null)
        {
            APIResponseDataModel<BranchDetails> resp = new APIResponseDataModel<BranchDetails>();
            try
            {
                var data = _branch.GetBranchDetails(branchName, BranchID, BranchOwnerID, BranchTypeID,StateID,DistrictID);
                resp.Code = 200;
                resp.Message = "";
                resp.Data = data.ToList();
                return Request.CreateResponse<APIResponseDataModel<BranchDetails>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<BranchDetails>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }


        [Route("getbranchlistddl")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<BranchDetails>))]
        public HttpResponseMessage GetBranchListDDL()
        {
            APIResponseDataModel<BranchMaster> resp = new APIResponseDataModel<BranchMaster>();
            try
            {
                var data = _branch.GetBranchListDDL();
                resp.Code = 200;
                resp.Message = "";
                resp.Data = data.ToList();
                return Request.CreateResponse<APIResponseDataModel<BranchMaster>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<BranchMaster>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }


        [Route("insertupdatebranch")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage InserUpdateBranch([FromBody] BranchCreateRequestModel request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                }
                var data = _branch.InsertUpdateBranch(request);

                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }


        }
        [Route("insertupdatebranchowner")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage InserUpdateBranchOwner(List<BranchOwnerCreateRequestModel> request)
        {
            CommonResponseDataModel resp = new CommonResponseDataModel();
            try
            {
                if (!ModelState.IsValid)
                {
                    resp.Code = 400;
                    resp.Message = JsonConvert.SerializeObject(ModelState);
                    return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.BadRequest, resp, "application/json");
                }
                resp = _branch.InsertUpdateBranchOwner(request);
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }

        [Route("getbranchType")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<BranchTypes>))]
        public HttpResponseMessage GetBranchType()
        {
            APIResponseDataModel<BranchTypes> resp = new APIResponseDataModel<BranchTypes>();
            try
            {
                var data = _branch.GetBranchTypes();
                resp.Code = 200;
                resp.Message = "";
                resp.Data = data.ToList();
                return Request.CreateResponse<APIResponseDataModel<BranchTypes>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<BranchTypes>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }


        [Route("getAllState")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<BranchTypes>))]
        public HttpResponseMessage GetAllState()
        {
            APIResponseDataModel<StateList> resp = new APIResponseDataModel<StateList>();
            try
            {
                var data = _branch.GetAllState();
                resp.Code = 200;
                resp.Message = "";
                resp.Data = data.ToList();
                return Request.CreateResponse<APIResponseDataModel<StateList>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<StateList>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }

        [Route("getAllDistrict")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<BranchTypes>))]
        public HttpResponseMessage GetAllDistrict(int? stateId = null)
        {
            APIResponseDataModel<DistrictDataModel> resp = new APIResponseDataModel<DistrictDataModel>();
            try
            {
                var data = _branch.GetAllDistrict(stateId);
                resp.Code = 200;
                resp.Message = "";
                resp.Data = data.ToList();
                return Request.CreateResponse<APIResponseDataModel<DistrictDataModel>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<DistrictDataModel>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }


        [Route("insertupdatebranchuser")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage InserUpdateBranchUser(BranchUserCreateRequestModel request)
        {
            CommonResponseDataModel resp = new CommonResponseDataModel();
            try
            {
                if (!ModelState.IsValid)
                {
                    resp.Code = 400;
                    resp.Message = JsonConvert.SerializeObject(ModelState);
                    return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.BadRequest, resp, "application/json");
                }
                resp = _branch.InsertUpdateBranchUser(request);
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }

        /// <summary>
        /// GET branch details 
        /// </summary>
        /// <param name="BranchID"></param>
        /// <returns></returns>
        [Route("getbranchusers")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<BranchUsersDetails>))]
        public HttpResponseMessage GetBranchUsers(int? BranchID = null)
        {
            APIResponseDataModel<BranchUsersDetails> resp = new APIResponseDataModel<BranchUsersDetails>();
            try
            {
                List<BranchUsersDetails> resObj = new List<BranchUsersDetails>();
                var data = _branch.GetBranchUsers(BranchID);
                resObj.Add(new BranchUsersDetails()
                {
                    Branch = data.Branch,
                    BranchUsers = data.BranchUsers
                });
                resp.Code = 200;
                resp.Message = "";
                resp.Data = resObj.ToList();
                return Request.CreateResponse<APIResponseDataModel<BranchUsersDetails>>(HttpStatusCode.OK, resp, "application/json");

            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<BranchUsersDetails>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }


        #region BranchQuery
        [Route("addBranchQuery")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage AddBranchQuery(CreateBranchQueryRequest request)
        {
            CommonResponseDataModel resp = new CommonResponseDataModel();
            try
            {
                if (!ModelState.IsValid)
                {
                    resp.Code = 400;
                    resp.Message = JsonConvert.SerializeObject(ModelState);
                    return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.BadRequest, resp, "application/json");
                }
                resp = _branch.InsertUpdateBranchQuery(request);
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }

        [Route("getbranchQueryDetails")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<BranchQueryResponseDataModel>))]
        public HttpResponseMessage GetBranchQueryDetails(int? BranchQueryId = null)
        {
            APIResponseDataModel<BranchQueryResponseDataModel> resp = new APIResponseDataModel<BranchQueryResponseDataModel>();
            try
            {
                var data = _branch.GetBranchQueryDetails(BranchQueryId);
                resp.Code = 200;
                resp.Message = "";
                resp.Data = data.ToList();
                return Request.CreateResponse<APIResponseDataModel<BranchQueryResponseDataModel>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<BranchQueryResponseDataModel>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }

        [Route("updateIsReadBranchQuery")]
        [HttpGet]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage UpdateIsReadBranchQuery(int BranchQueryID,bool IsRead)
        {
            CommonResponseDataModel resp = new CommonResponseDataModel();
            try
            {
                if (!ModelState.IsValid)
                {
                    resp.Code = 400;
                    resp.Message = JsonConvert.SerializeObject(ModelState);
                    return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.BadRequest, resp, "application/json");
                }
                resp = _branch.UpdateIsReadBranchQuery(BranchQueryID,IsRead);
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<CommonResponseDataModel>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }


        #endregion
    }
}
