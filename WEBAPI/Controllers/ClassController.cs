﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.Code;

namespace WEBAPI.Controllers
{
    //[CustomAuthorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    [RoutePrefix("api/v1/class")]
    public class ClassController : ApiController
    {

        IClass _class;
        IGlobal _global;

        #region CTOR
        public ClassController()
        {
            _global = new Global();
            _class = new ClassBusiness(_global);
        }
        #endregion

        [Route("getclassdetails")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<ClassDataModel>))]
        public HttpResponseMessage getclassdetails(String className = null, int? classID = null)
        {
            APIResponseDataModel<ClassDataModel> resp = new APIResponseDataModel<ClassDataModel>();
            try
            {
                var data = _class.GetClass(classID, className);
                resp.Code = 200;
                resp.Message = "";
                resp.Data = data.ToList();
                return Request.CreateResponse<APIResponseDataModel<ClassDataModel>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<ClassDataModel>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }

        [Route("insertupdateclass")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage insertupdateclass([FromBody] ClassRequestModel request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                }
                var data = _class.InsertUpdateClass(request);

                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }


        }

        [Route("updatestatusclass")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage updatestatusclass([FromBody] StatusRequestModel request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                }
                var data = _class.UpdateStatusClass(request);

                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }


        }
    }
}
