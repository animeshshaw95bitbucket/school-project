﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.Code;

namespace WEBAPI.Controllers
{
    //[CustomAuthorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    [RoutePrefix("api/v1/session")]
    public class SessionController : ApiController
    {
        ISession _session;
        IGlobal _global;
        #region CTOR
        public SessionController()
        {
            _global = new Global();
            _session = new SessionBusiness(_global);
        }
        #endregion
        [Route("getsessiondetails")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<SessionMaster>))]
        public HttpResponseMessage getsessiondetails(String sessionName = null, int? sessionID = null, String classCode = null)
        {
            APIResponseDataModel<SessionMaster> resp = new APIResponseDataModel<SessionMaster>();
            try
            {
                var data = _session.GetSession(sessionID, sessionName, classCode);
                resp.Code = 200;
                resp.Message = "";
                resp.Data = data.ToList();
                return Request.CreateResponse<APIResponseDataModel<SessionMaster>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<SessionMaster>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }

        [Route("insertupdatesession")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage insertupdatesession([FromBody] SessionRequestModel request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                }
                var data = _session.InsertUpdateSession(request);

                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }


        }

        [Route("updatestatussession")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage updatestatussession([FromBody] StatusRequestModel request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                }
                var data = _session.UpdateStatusSession(request);

                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }


        }
    }
}
