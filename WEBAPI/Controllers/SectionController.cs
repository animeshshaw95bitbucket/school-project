﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.Code;

namespace WEBAPI.Controllers
{
    //[CustomAuthorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    [RoutePrefix("api/v1/section")]
    public class SectionController : ApiController
    {
        ISection _section;
        IGlobal _global;

        #region CTOR
        public SectionController()
        {
            _global = new Global();
            _section = new SectionBusiness(_global);
        }
        #endregion

        [Route("getsectiondetails")]
        [HttpGet]
        [ResponseType(typeof(APIResponseDataModel<SectionDataModel>))]
        public HttpResponseMessage getsectiondetails(String sectionName = null, int? sectionID = null, String sectionCode = null)
        {
            APIResponseDataModel<SectionDataModel> resp = new APIResponseDataModel<SectionDataModel>();
            try
            {
                var data = _section.GetSection(sectionID, sectionName, sectionCode);
                resp.Code = 200;
                resp.Message = "";
                resp.Data = data.ToList();
                return Request.CreateResponse<APIResponseDataModel<SectionDataModel>>(HttpStatusCode.OK, resp, "application/json");
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.Message.ToString();
                return Request.CreateResponse<APIResponseDataModel<SectionDataModel>>(HttpStatusCode.InternalServerError, resp, "application/json");
            }
        }

        [Route("insertupdatesection")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage insertupdatesection([FromBody] SectionRequestModel request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                }
                var data = _section.InsertUpdateSection(request);

                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }


        }

        [Route("updatestatussection")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage updatestatussection([FromBody] StatusRequestModel request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                }
                var data = _section.UpdateStatusSection(request);

                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }


        }
    }
}
