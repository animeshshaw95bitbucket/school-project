﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;

namespace WEBAPI.Controllers
{
    //[CustomAuthorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    [RoutePrefix("api/v1/user")]
    public class UserController : ApiController
    {
        private readonly IUser _user;
        IGlobal _global;
        #region CTOR
        public UserController( IUser user,IGlobal global)
        {
            _user = user;
           _global = global;
        }
        #endregion
        [Route("AddUser")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage AddUser([FromBody]AddUserRequestModel model)
        {
            try
            {
                var data = _user.AddUser(model);
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.InternalServerError); 
            }
        }

        [Route("updateUserPassword")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage updateUserPassword([FromBody] AddUserRequestModel model)
        {
            try
            {
                var data = _user.UpdateUserPassword(model);
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        
        [Route("GetUsersDetails")]
        public HttpResponseMessage GetUsersDetails([FromUri] int? UserID=null, int? BranchId = null, string UserName = null)
        {
            try
            {
                var data = _user.GetUsersDetails(UserID, BranchId, UserName);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(data));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("GetUserByUserName")]
        public HttpResponseMessage GetUserByUserName([FromUri] string UserName)
        {
            try
            {
                var data = _user.GetUserByUserName(UserName);

                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(data));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [Route("insertupdateuserrole")]
        [HttpGet]
        public HttpResponseMessage InsertUpdateUserrole([FromUri] int UserID , string RoleIds)
        {
            try
            {
                var data = _user.InsertUpdteUserRole(UserID,RoleIds);
               
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(data));
            }
            catch (Exception ex)
            {


                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("GetUserProfile")]
        public HttpResponseMessage GetUserProfile([FromUri] int? UserID = null,string UserName = null)
        {
            try
            {
                var data = _user.GetUserProfile(UserID,UserName);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(data));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("UpdateProfile")]
        [HttpPost]
        [ResponseType(typeof(CommonResponseDataModel))]
        public HttpResponseMessage UpdateProfile([FromBody]UpdateProfileRequestModel model)
        {
            try
            {
                var data = _user.UpdateProfile(model);
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
