﻿using System;
using System.Configuration;

namespace WEB.Code
{
    /// <summary>
    /// 
    /// </summary>
    public static class GlobalConstants
    {
        // Public objects.       
        public static Int32 DefaultPageListSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageListSize"]);
        public static String APIURL = ConfigurationManager.AppSettings["APIURL"];
        public static String AuthTokenExpiry = ConfigurationManager.AppSettings["AuthTokenExpiry"];
        public static String audienceID = ConfigurationManager.AppSettings["audienceID"];
        public static String issuer = ConfigurationManager.AppSettings["issuer"];
        public static String signKey = ConfigurationManager.AppSettings["signKey"];
        public static String GeneralErrorMessgae = "An error occurred.";
        public static String DefaultAPITimeout = ConfigurationManager.AppSettings["DefaultAPITimeout"];
        public static String DomainURL = ConfigurationManager.AppSettings["WebSiteURL"];
    }
}
