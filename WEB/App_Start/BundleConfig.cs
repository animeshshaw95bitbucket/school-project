﻿using System.Web.Optimization;

namespace WEB
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/RenderJs").Include(
                        "~/Content/vendor/jquery/jquery.min.js",
                        "~/Content/vendor/bootstrap/js/bootstrap.bundle.min.js",
                        "~/Content/vendor/jquery-easing/jquery.easing.min.js",
                        //"~/Content/js/jquery.validate.min.js",
                        //"~/Content/js/myscript.js",
                        "~/Content/js/sb-admin-2.min.js"
                       // "~/Content/vendor/chart.js/Chart.min.js",
                      //  "~/Content/js/demo/chart-area-demo.js",
                       // "~/Content/js/demo/chart-pie-demo.js"
                        ));



            bundles.Add(new StyleBundle("~/bundel/RenderCss").Include(
                      "~/Content/vendor/fontawesome-free/css/all.min.css",
                      "~/Content/css/sb-admin-2.css"));
            // BundleTable.EnableOptimizations = true;
        }
    }
}
