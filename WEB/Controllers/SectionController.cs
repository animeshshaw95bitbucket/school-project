﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.Core.Common;
using Web.Core.Interface;
using Web.Core.ViewModel;

namespace WEB.Controllers
{
    public class SectionController : Controller
    {
        IGlobal _global;
        CommonServices _service;

        public SectionController(IGlobal global)
        {
            _global = global;
            _service = new CommonServices(_global);

        }

        // GET: Section
        public ActionResult Index()
        {
            var _SectionDetails = _service.Get<ApiResponseViewModel<SectionDataModel>>(_global.ApiUrl() + "/api/v1/section/getsectiondetails").Item3;
            return View(_SectionDetails.Data);
        }

        //Get Section By Name
        [HttpPost]
        public ActionResult Index(String SectionName)
        {
            var param = string.IsNullOrEmpty(SectionName) ? "" : "?SectionName=" + SectionName;
            var _SessionDetails = _service.Get<ApiResponseViewModel<SectionDataModel>>(_global.ApiUrl() + "/api/v1/section/getsectiondetails" + param).Item3;
            return View(_SessionDetails.Data);
        }

        [HttpGet]
        public ActionResult AddUpdateSection(int? SectionID = null)
        {
            SectionInsertUpdateRequestModel model = new SectionInsertUpdateRequestModel();

            var _ClassList = _service.Get<ApiResponseViewModel<ClassMasterDataModel>>(_global.ApiUrl() + "/api/v1/class/getclassdetails").Item3;
            var ddlClass = JsonConvert.DeserializeObject<ApiResponseViewModel<ClassMasterDataModel>>(JsonConvert.SerializeObject(_ClassList)).Data;
            model.ddlClass = ddlClass;

            if (SectionID.HasValue)
            {
                var _SectionDetails = _service.Get<object>(_global.ApiUrl() + "/api/v1/section/getsectiondetails?sectionID=" + SectionID);
                var outout = JsonConvert.DeserializeObject<ApiResponseViewModel<SectionDataModel>>(JsonConvert.SerializeObject(_SectionDetails.Item3))?.Data?.FirstOrDefault();
                if (outout != null)
                {
                    model.SectionID = outout.SectionID;
                    model.SectionName = outout.SectionName;
                    model.SectionDesc = outout.SectionDescription;
                    model.IsActive = outout.IsActive ? 1 : 0;
                    model.ClassID = outout.ClassID;
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult AddUpdateSection(SectionInsertUpdateRequestModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var identity = User.Identity as ClaimsIdentity;
            var userID = identity.Claims.Where(m => m.Type == "UserID").FirstOrDefault().Value ?? "0";
            model.CreatedBy = Convert.ToInt32(userID);

            var jSon = new JavaScriptSerializer().Serialize(model);
            var res = _service.Post<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/section/insertupdatesection", jSon);

            if (res.Item3 != null && !res.Item3.Successful)
            {
                ViewBag.Error = "Unable to save section!";
            }

            if (res.Item3 != null && res.Item3.ReturnID > 0)
            {
                model.SectionID = res.Item3.ReturnID;

                return RedirectToAction("Index");
            }

            return View(model);
        }
    }
}