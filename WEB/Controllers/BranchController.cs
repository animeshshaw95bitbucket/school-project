﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.Core.Common;
using Web.Core.Interface;
using Web.Core.ViewModel;

namespace WEB.Controllers
{
    public class BranchController : Controller
    {
        IGlobal _global;
        CommonServices _service;

        public BranchController(IGlobal global)
        {
            _global = global;
            _service = new CommonServices(_global);

        }

        // GET: Branch
        [HttpGet]
        public ActionResult Index()
        {
            var _BranchDetails = _service.Get<ApiResponseViewModel<BranchDetailsDataModel>>(_global.ApiUrl() + "/api/v1/branch/getbranchdetails").Item3;

            //var branch = JsonConvert.DeserializeObject<ApiResponseViewModel<BranchDetailsDataModel>>(JsonConvert.SerializeObject(_BranchDetails.Item3)).Data;
            // var edit = branch.Branch;
            //  branch.BranchOwners = branch.BranchOwners ?? new List<BranchOwnerDataModel>();
            return View(_BranchDetails.Data);
        }

        [HttpPost]
        public ActionResult Index(String BranchName)
        {
            var param = string.IsNullOrEmpty(BranchName) ? "" : "?branchName=" + BranchName;
            var _BranchDetails = _service.Get<ApiResponseViewModel<BranchDetailsDataModel>>(_global.ApiUrl() + "/api/v1/branch/getbranchdetails" + param).Item3;

            //var branch = JsonConvert.DeserializeObject<ApiResponseViewModel<BranchDetailsDataModel>>(JsonConvert.SerializeObject(_BranchDetails.Item3)).Data;
            // var edit = branch.Branch;
            //  branch.BranchOwners = branch.BranchOwners ?? new List<BranchOwnerDataModel>();
            return View(_BranchDetails.Data);
        }
        public ActionResult AddUpdateBranch(int? BranchID = null)
        {
            // BranchID = 26;
            BranchDetailsInsertUpdateRequestModel model = new BranchDetailsInsertUpdateRequestModel();
            model.Owners = new List<BranchCreateOwnerRequestModel>();
            if (BranchID.HasValue)
            {

                var _BranchDetails = _service.Get<object>(_global.ApiUrl() + "/api/v1/branch/getbranchdetails?BranchID=" + BranchID);
                var branch = JsonConvert.DeserializeObject<ApiResponseViewModel<BranchDetailsDataModel>>(JsonConvert.SerializeObject(_BranchDetails.Item3)).Data.FirstOrDefault();
                var edit = branch.Branch;
                branch.BranchOwners = branch.BranchOwners ?? new List<BranchOwnerDataModel>();
                model = new BranchDetailsInsertUpdateRequestModel()
                {
                    GP_Municipality = edit.GP_Municipality,
                    Block = edit.Block,
                    BranchID = edit.BranchID,
                    BranchAddress = edit.BranchAddress,
                    BranchCode = edit.BranchCode,
                    BranchName = edit.BranchName,
                    BranchTypeID = edit.BranchTypeID,
                    DistrictID = edit.DistrictID,
                    IsActive = edit.IsActive,
                    PostOffice = edit.PostOffice,
                    StateID = edit.StateID,
                    Vill = edit.Vill,
                };
                model.Owners = new List<BranchCreateOwnerRequestModel>();
                int tempId = 0;
                foreach (var item in branch.BranchOwners)
                {
                    model.Owners.Add(new BranchCreateOwnerRequestModel()
                    {
                        TempOwnerID = tempId,
                        AadharNo = item.AadharNo,
                        BranchID = item.BranchID,
                        BranchOwnerID = item.BranchOwnerID,
                        Designation = item.Designation,
                        DOB = item.DOB,
                        DOJ = item.DOJ,
                        EmailAddress = item.EmailAddress,
                        IsActive = item.IsActive,
                        OwnerName = item.OwnerName,
                        PanNo = item.PanNo,
                        PrimaryContactNo = item.PrimaryContactNo,
                        QualificationID = item.QualificationID,
                        SecondaryContactNo = item.SecondaryContactNo,
                        VoterNo = item.VoterNo


                    });
                    tempId++;
                }
            }







            var branchType = _service.Get<object>(_global.ApiUrl() + "/api/v1/branch/getbranchType");
            var State = _service.Get<object>(_global.ApiUrl() + "/api/v1/branch/getAllState");
            var District = _service.Get<object>(_global.ApiUrl() + "/api/v1/branch/getAllDistrict");

            var branchTypeList = JsonConvert.DeserializeObject<ApiResponseViewModel<BranchTypeDataModel>>(JsonConvert.SerializeObject(branchType.Item3)).Data;
            var Statelist = JsonConvert.DeserializeObject<ApiResponseViewModel<StateDataModel>>(JsonConvert.SerializeObject(State.Item3)).Data;
            var distructList = JsonConvert.DeserializeObject<ApiResponseViewModel<DistrictDataModel>>(JsonConvert.SerializeObject(District.Item3)).Data;

            model.BranchTypeList = branchTypeList;
            model.StateList = Statelist;
            model.DistrictList = distructList;




            TempData["BranchMaster"] = model;
            ViewBag.LastId = BranchID.HasValue ? model.Owners.Count() : 0;
            return View(model);
        }
        [HttpPost]

        public ActionResult AddUpdateBranch(BranchDetailsInsertUpdateRequestModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            var branch = new BranchDetailsInsertUpdateRequestModel();
            model.BranchCode = "";
            //  model.GP_Municipality = "GPM";
            if (TempData.ContainsKey("BranchMaster"))
            {
                branch = TempData["BranchMaster"] as BranchDetailsInsertUpdateRequestModel;
            }
            TempData.Keep();
            model.DistrictList = branch.DistrictList;
            model.BranchTypeList = branch.BranchTypeList;
            model.StateList = branch.StateList;
            model.Owners = branch.Owners;




            var jSon = new JavaScriptSerializer().Serialize(model);
            var res = _service.Post<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/branch/insertupdatebranch", jSon);
            //if (TempData.ContainsKey("BranchMaster"))
            //{
            //    branch = TempData["BranchMaster"] as BranchDetailsInsertUpdateRequestModel;
            //}
            //TempData.Keep();
            //model.DistrictList = branch.DistrictList;
            //model.BranchTypeList = branch.BranchTypeList;
            //model.StateList = branch.StateList;
            //model.Owners = branch.Owners;
            if (res.Item3 != null && res.Item3.ReturnID > 0)
            {
                model.BranchID = res.Item3.ReturnID;
                model.BranchCode = res.Item3.ExtraFiled ?? string.Empty;
                ViewBag.LastId = model.Owners.Count();

                return View(model);
            }
            ViewBag.Error = "Unable to add new branch!";
            ViewBag.LastId = model.Owners.Count();

            return View(model);
        }

        public ActionResult AddNewOwner(int LastId)
        {
            ViewBag.LastId = LastId;

            return PartialView("_InsertUpdateBranchOwner", new BranchCreateOwnerRequestModel() { OwnerIndexNo = LastId });

        }
        [HttpPost]
        public ActionResult InsertUpdateBranchOwner(List<BranchCreateOwnerRequestModel> model)
        {
            var jSon = new JavaScriptSerializer().Serialize(model);
            var res = _service.Post<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/branch/insertupdatebranchowner", jSon);
            if (res.Item3 != null && res.Item3.ReturnID > 0)
                return Json(new { IsSucess = true }, JsonRequestBehavior.AllowGet);

            return Json(new { IsSucess = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BranchUsers()
        {
            var _BranchDetails = _service.Get<ApiResponseViewModel<BranchUserMappingDeatils>>(_global.ApiUrl() + "/api/v1/branch/getbranchusers").Item3;
            BranchUserMappingDeatils data = _BranchDetails.Data.FirstOrDefault();

            return View(data);
        }

        public ActionResult BranchQuery()
        {
            var _branchQuery = _service.Get<ApiResponseViewModel<BranchQueryViewModel>>(_global.ApiUrl() + "/api/v1/branch/getbranchQueryDetails").Item3;

            return View(_branchQuery?.Data ?? new List<BranchQueryViewModel>());
        }

        [HttpGet]
        public ActionResult UpdateIsReadBranchQuery(int branchQueryID, bool isRead)
        {
            var res = _service.Get<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/branch/updateIsReadBranchQuery?BranchQueryID=" + branchQueryID + "&IsRead=" + isRead).Item3;
            if (res != null && res.ReturnID > 0)
                return Json(new { IsSucess = true }, JsonRequestBehavior.AllowGet);
            return Json(new { IsSucess = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentQuery()
        {
           var jSon = new JavaScriptSerializer().Serialize(new StudentQueryRequestDataModel());
            var _studentQuery = _service.Get<ApiResponseViewModel<StudentQueryViewModel>>(_global.ApiUrl() + "/api/v1/student/Query/All").Item3;

            return View(_studentQuery?.Data ?? new List<StudentQueryViewModel>());
           // return View();
        }

        [HttpGet]
        public ActionResult UpdateIsReadStudentQuery(int studentQueryID, bool isRead)
        {
            var res = _service.Get<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/student/Query/IsRead?StudentQueryID=" + studentQueryID + "&IsRead=" + isRead).Item3;
            if (res != null && res.ReturnID > 0)
                return Json(new { IsSucess = true }, JsonRequestBehavior.AllowGet);
            return Json(new { IsSucess = false }, JsonRequestBehavior.AllowGet);
        }
    }
}