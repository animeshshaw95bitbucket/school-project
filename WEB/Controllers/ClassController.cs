﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.Core.Common;
using Web.Core.Interface;
using Web.Core.ViewModel;

namespace WEB.Controllers
{
    public class ClassController : Controller
    {
        IGlobal _global;
        CommonServices _service;

        public ClassController(IGlobal global)
        {
            _global = global;
            _service = new CommonServices(_global);

        }

        // GET: Class
        public ActionResult Index()
        {
            var _ClassDetails = _service.Get<ApiResponseViewModel<ClassMasterDataModel>>(_global.ApiUrl() + "/api/v1/class/getclassdetails").Item3;
            return View(_ClassDetails.Data);
        }

        //Get Class By Name
        [HttpPost]
        public ActionResult Index(String ClassName)
        {
            var param = string.IsNullOrEmpty(ClassName) ? "" : "?className=" + ClassName;
            var _ClassDetails = _service.Get<ApiResponseViewModel<ClassMasterDataModel>>(_global.ApiUrl() + "/api/v1/class/getclassdetails" + param).Item3;
            return View(_ClassDetails.Data);
        }

        public ActionResult AddUpdateClass(int? ClassID = null)
        {
            ClassInsertUpdateRequestModel model = new ClassInsertUpdateRequestModel();

            var _SessionList = _service.Get<ApiResponseViewModel<SessionMasterDataModel>>(_global.ApiUrl() + "/api/v1/session/getsessiondetails").Item3;
            var ddlSessions = JsonConvert.DeserializeObject<ApiResponseViewModel<SessionMasterDataModel>>(JsonConvert.SerializeObject(_SessionList)).Data;
            model.ddlSessions = ddlSessions;

            if (ClassID.HasValue)
            {
                var _ClassDetails = _service.Get<object>(_global.ApiUrl() + "/api/v1/class/getclassdetails?ClassID=" + ClassID);
                var outout = JsonConvert.DeserializeObject<ApiResponseViewModel<ClassMasterDataModel>>(JsonConvert.SerializeObject(_ClassDetails.Item3))?.Data?.FirstOrDefault();
                if (outout != null)
                {
                    model.ClassID = outout.ClassID;
                    model.ClassName = outout.ClassName;
                    model.ClassDescription = outout.ClassDescription;
                    model.IsActive = outout.IsActive ? 1 : 0;
                    model.SessionID = outout.SessionID;
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult AddUpdateClass(ClassInsertUpdateRequestModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var identity = User.Identity as ClaimsIdentity;
            var userID = identity.Claims.Where(m => m.Type == "UserID").FirstOrDefault().Value ?? "0";
            model.CreatedBy = Convert.ToInt32(userID);

            var jSon = new JavaScriptSerializer().Serialize(model);
            var res = _service.Post<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/class/insertupdateclass", jSon);

            if (res.Item3 != null && !res.Item3.Successful)
            {
                ViewBag.Error = "Unable to save class!";
            }

            if (res.Item3 != null && res.Item3.ReturnID > 0)
            {
                model.ClassID = res.Item3.ReturnID;

                return RedirectToAction("Index");
            }

            return View(model);
        }

    }
}