﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Core.Common;
using Web.Core.Interface;

namespace WEB.Controllers
{
    public class StudentController : Controller
    {
        IGlobal _global;
        CommonServices _service;

        public StudentController(IGlobal global)
        {
            _global = global;
            _service = new CommonServices(_global);

        }

        // GET: Student
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
    }
}