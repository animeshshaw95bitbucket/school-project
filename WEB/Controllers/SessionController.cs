﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.Core.Common;
using Web.Core.Interface;
using Web.Core.ViewModel;

namespace WEB.Controllers
{
    public class SessionController : Controller
    {
        IGlobal _global;
        CommonServices _service;

        public SessionController(IGlobal global)
        {
            _global = global;
            _service = new CommonServices(_global);

        }

        //Get Session
        [HttpGet]
        public ActionResult Index()
        {
            var _SessionDetails = _service.Get<ApiResponseViewModel<SessionMasterDataModel>>(_global.ApiUrl() + "/api/v1/session/getsessiondetails").Item3;
            return View(_SessionDetails.Data);
        }

        //Get Session By Name
        [HttpPost]
        public ActionResult Index(String SessionName)
        {
            var param = string.IsNullOrEmpty(SessionName) ? "" : "?sessionName=" + SessionName;
            var _SessionDetails = _service.Get<ApiResponseViewModel<SessionMasterDataModel>>(_global.ApiUrl() + "/api/v1/session/getsessiondetails" + param).Item3;
            return View(_SessionDetails.Data);
        }

        public ActionResult AddUpdateSession(int? SessionID = null)
        {
            SessionInsertUpdateRequestModel model = new SessionInsertUpdateRequestModel();
            if (SessionID.HasValue)
            {
                var _SessionDetails = _service.Get<object>(_global.ApiUrl() + "/api/v1/session/getsessiondetails?SessionID=" + SessionID);
                var outout  = JsonConvert.DeserializeObject<ApiResponseViewModel<SessionMasterDataModel>>(JsonConvert.SerializeObject(_SessionDetails.Item3))?.Data?.FirstOrDefault();
                if (outout != null)
                {
                    model.SessionID = outout.SessionID;
                    model.SessionName = outout.SessionName;
                    model.SessionDesc = outout.SessionDescription;
                    model.IsActive = outout.IsActive?1:0;
                    model.PrincipalSignaturePath = outout.PrincipalSignature;
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult AddUpdateSession(SessionInsertUpdateRequestModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var identity = User.Identity as ClaimsIdentity;
            var userID = identity.Claims.Where(m => m.Type == "UserID").FirstOrDefault().Value ?? "0";
            model.CreatedBy =Convert.ToInt32(userID);

            var jSon = new JavaScriptSerializer().Serialize(model);
            var res = _service.Post<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/session/insertupdatesession", jSon);

            if (res.Item3 != null && !res.Item3.Successful)
            {
                ViewBag.Error = "Unable to save session!";
            }

            if (res.Item3 != null && res.Item3.ReturnID > 0)
            {
                model.SessionID = res.Item3.ReturnID;

                return RedirectToAction("Index");
            }

            return View(model);
        }

    }
}