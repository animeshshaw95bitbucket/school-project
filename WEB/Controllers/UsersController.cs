﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.Core.Common;
using Web.Core.Interface;
using Web.Core.ViewModel;
using WEB.Code;

namespace WEB.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        IGlobal _global;

        CommonServices _service;
        public UsersController(IGlobal global)
        {
            _global = global;
            _service = new CommonServices(_global);

        }

        [HttpGet]
        public ActionResult Index()
        {
            var _BranchDetails = _service.Get<ApiResponseViewModel<BranchMasterDataModel>>(_global.ApiUrl() + "/api/v1/branch/GetBranchListDDL").Item3;

            var res1 = _service.Get<object>(_global.ApiUrl() + "/api/v1/user/GetUsersDetails");

            var model = JsonConvert.DeserializeObject<UserResponseTracker>((String)res1.Item3);

            UserDetailsDataModel _itemUser = new UserDetailsDataModel();
            List<UserDetailsDataModel> users = new List<UserDetailsDataModel>();
            foreach (var item in model.Users)
            {
                _itemUser = item;
                var roleDetails = new List<RoleDataModel>();
                var mappedRole = model.UserRole.Where(m => m.UserID == item.UserID);
                foreach (var itm in mappedRole)
                {
                    var role = model.Roles.Where(m => m.RoleID == itm.RoleID).FirstOrDefault();
                    roleDetails.Add(role);

                }
                _itemUser.UserRoles = roleDetails;
                users.Add(_itemUser);
            }


            model.Users = users;
            model.BranchList = _BranchDetails.Data;

            TempData["RoleMaster"] = model.Roles;
            TempData["UserRole"] = model.UserRole;
            TempData["BranchList"] = model.BranchList;

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(Int32? BranchId, String UserName)
        {
            var _BranchDetails = _service.Get<ApiResponseViewModel<BranchMasterDataModel>>(_global.ApiUrl() + "/api/v1/branch/GetBranchListDDL").Item3;
            var _params = BranchId != null && BranchId > 0 ? "?BranchId=" + BranchId : "";
            _params += UserName != "" ? _params == "" ? "?UserName=" + UserName : "&UserName=" + UserName : "";
            var res1 = _service.Get<object>(_global.ApiUrl() + "/api/v1/user/GetUsersDetails" + _params);

            var model = JsonConvert.DeserializeObject<UserResponseTracker>((String)res1.Item3);

            UserDetailsDataModel _itemUser = new UserDetailsDataModel();
            List<UserDetailsDataModel> users = new List<UserDetailsDataModel>();
            foreach (var item in model.Users)
            {
                _itemUser = item;
                var roleDetails = new List<RoleDataModel>();
                var mappedRole = model.UserRole.Where(m => m.UserID == item.UserID);
                foreach (var itm in mappedRole)
                {
                    var role = model.Roles.Where(m => m.RoleID == itm.RoleID).FirstOrDefault();
                    roleDetails.Add(role);

                }
                _itemUser.UserRoles = roleDetails;
                users.Add(_itemUser);
            }


            model.Users = users;
            model.BranchList = _BranchDetails.Data;

            TempData["RoleMaster"] = model.Roles;
            TempData["UserRole"] = model.UserRole;
            TempData["BranchList"] = model.BranchList; // Not needed will need to remove it
            return View(model);
        }

        [HttpGet]
        public ActionResult AddUpdateuserModal(int UserID)
        {
            UserResponseTracker model = new UserResponseTracker();



            if (TempData.ContainsKey("RoleMaster"))
                model.Roles = TempData["RoleMaster"] as List<RoleDataModel>;
            if (TempData.ContainsKey("UserRole"))
                model.UserRole = TempData["UserRole"] as List<UserRoleDataModel>;

            TempData.Keep();
            model.UserRole = model.UserRole.Where(m => m.UserID == UserID);
            ViewBag.UserID = UserID;
            return View(model);
        }

        [HttpPost]
        public ActionResult AddUpdateUserModal(int UserID, string userRoleIds)
        {
            // userRoleIds = userRoleIds.Remove(0,1 );
            var res1 = _service.Get<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/user/insertupdateuserrole?UserId=" + UserID + "&RoleIds=" + userRoleIds);

            return Json(new { IsSucess = true }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Profile()
        {

            var identity = User.Identity as ClaimsIdentity;
            var userID = identity.Claims.Where(m => m.Type == "UserID").FirstOrDefault().Value ?? "0";

            var res1 = _service.Get<object>(_global.ApiUrl() + "/api/v1/user/GetUserProfile?UserID=" + userID);

            var _userDetails = JsonConvert.DeserializeObject<UserDetailsDataModel>((String)res1.Item3);

            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(Server.MapPath("~/Content/profile_img"));
            var objFileList = dir.GetFiles().Where(x => x.Name.Contains(_userDetails.UserID.ToString()));
            if (objFileList != null && objFileList.Any())
            {
                _userDetails.ProfileImageUrl = GlobalConstants.DomainURL + "Content/profile_img/" + objFileList.FirstOrDefault().Name;
            }

            return View(_userDetails);
        }

        [HttpPost]
        public ActionResult UpdateProfile(UpdateProfileRequestModel model)
        {
            var jSon = new JavaScriptSerializer().Serialize(model);
            var res = _service.Post<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/user/UpdateProfile", jSon);

            if (res.Item3.Successful)
            {

                System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(Server.MapPath("~/Content/profile_img"));
                var objFileList = dir.GetFiles().Where(x => x.Name.Contains(model.UserID.ToString()));
                if (objFileList != null && objFileList.Any())
                {
                    var objFile = objFileList.FirstOrDefault();

                    var _comPath = Path.Combine(Server.MapPath("~/Content/profile_img/"), objFile.Name);
                    var _comNewPath = Path.Combine(Server.MapPath("~/Content/profile_img/"), model.UserID + objFile.Extension);
                    System.IO.File.Move(_comPath, _comNewPath);
                }

            }
            return RedirectToAction("Profile", "Users");
        }

        public JsonResult UploadProfile()
        {
            string _imgname = "";
            var identity = User.Identity as ClaimsIdentity;
            var userID = identity.Claims.Where(m => m.Type == "UserID").FirstOrDefault().Value ?? "0";

            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var pic = System.Web.HttpContext.Current.Request.Files["flProfile"];
                    if (pic.ContentLength > 0)
                    {
                        var _ext = Path.GetExtension(pic.FileName);
                        _imgname = userID + _ext;

                        var _comPath = Path.Combine(Server.MapPath("~/Content/profile_img/"), _imgname);
                        _imgname = GlobalConstants.DomainURL + _imgname;
                        

                        var path = _comPath;
                        // Saving Image in Original Mode
                        pic.SaveAs(path);

                        // resizing image
                        MemoryStream ms = new MemoryStream();
                        WebImage img = new WebImage(_comPath);

                        if (img.Width > 200)
                            img.Resize(200, 200);
                        img.Save(_comPath);
                        // end resize
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }
    }

    public class UserResponseTracker
    {

        public IEnumerable<UserDetailsDataModel> Users { get; set; }

        [JsonProperty]

        public IEnumerable<RoleDataModel> Roles { get; set; }

        [JsonProperty]

        public IEnumerable<UserRoleDataModel> UserRole { get; set; }

        public List<BranchMasterDataModel> BranchList { get; set; }
    }

}