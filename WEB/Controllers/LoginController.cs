﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UtilityService;
using Web.Core.Business;
using Web.Core.Common;
using Web.Core.Interface;
using Web.Core.ViewModel;
using WEB.Code;

namespace WEB.Controllers
{
    public class LoginController : Controller
    {
        IGlobal _global;
        ILogin _login;
        CommonServices _service;
        #region CTOR
        public LoginController(IGlobal global, ILogin login)
        {
            _global = global;
            _login = login;
            _service = new CommonServices(_global);
        }
        #endregion
        // GET: Login
        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            LogOff(false);
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(LoginRequestViewModel request, string returnUrl)
        {
            string fileName = "debabratakhanra17@gmail.com.*";
            string path = GlobalConstants.DomainURL + "Content/profile_img/";
            string result;

            result = Path.GetFileName(fileName);
           
            if (!ModelState.IsValid)
            {
                return View(request);
            }
            var resp = _login.Login(request);

            if (resp.Successfull)
            {
                Session["UserAuthToken"] = resp.Token;
                var ctx = Request.GetOwinContext();
                var authenticationManager = ctx.Authentication;
                var claimIdenties = new ClaimsIdentity(resp.claims, DefaultAuthenticationTypes.ApplicationCookie);
                authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = request.RememberMe, IssuedUtc = DateTime.UtcNow, ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(_global.AuthTokenExpiry())) }, claimIdenties);
                if (!string.IsNullOrEmpty(returnUrl) && returnUrl != "/")
                    return Redirect(returnUrl);
                else
                    return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.isError = true;
                ViewBag.Message = resp.Message;
                return View(request);
            }
        }

        public ActionResult Registration()
        {
            ViewBag.isUserExist = false;
            var model = new RegistrationModel();
            var _BranchDetails = _service.Get<ApiResponseViewModel<BranchMasterDataModel>>(_global.ApiUrl() + "/api/v1/branch/GetBranchListDDL").Item3;

            model.BranchList = _BranchDetails.Data;
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Registration(RegistrationModel model)
        {

            var jSon = new JavaScriptSerializer().Serialize(model);
            var res = _service.Post<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/user/adduser", jSon);
            if (res.Item3.ReturnID > 0)
            {
                if (model.BranchId > 0)
                {
                    var BranchUsers = new JavaScriptSerializer().Serialize(new { BranchID = model.BranchId.Value, UserID = res.Item3.ReturnID });
                    var resModel = _service.Post<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/branch/insertupdatebranchuser", BranchUsers);
                    return RedirectToAction("Index", "Branch");
                }

                return RedirectToAction("Index");
            }
            else if (res.Item3.Message != null)
            {
                ViewBag.isUserExist = true;
                ViewBag.message = res.Item3.Message;
            }
            return View(model);

        }

        //[HttpPost]
        public ActionResult LogOff(bool? isLogout)
        {
            // Setting.    
            var ctx = Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            // Sign Out.    
            authenticationManager.SignOut();
            //Session Clear
            Session.Clear();
            Session.Abandon();
            if (isLogout == true)
                return RedirectToAction("Index", "Login");
            return null;
        }

        public ActionResult ForgotPassword()
        {
            ViewBag.IsUser = false;
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string userName)
        {

            Match match = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(userName);
            if (!match.Success)
            {
                return View();
            }
            var isUser = false;
            var res1 = _service.Get<object>(_global.ApiUrl() + "/api/v1/user/GetUserByUserName?UserName=" + userName);

            var model = JsonConvert.DeserializeObject<UserResponseTracker>((String)res1.Item3);
            isUser = model.Users.FirstOrDefault().UserID > 0 ? true : false;
            ViewBag.IsUser = isUser;
            if (isUser)
            {
                var strBody = Helper.BuildResetPasswordEmailBody(userName);
                var isSentMail = Helper.SendVerifyEmail("deba.somu@gmail.com", userName.Trim(), "Reset Pasword", strBody, true);



                ViewBag.Message = isSentMail ? "Email has been sent to your " + userName : "Unable to sent mail! Please try again";
                // sentEmail();
            }
            else
            {

                ViewBag.Message = userName + "- user not found, Please check your user name! ";
            }
            return View();
        }

        public ActionResult ResetPassword(string str)
        {
            var byteArr = Convert.FromBase64String(str);
            string decodedString = Encoding.UTF8.GetString(byteArr);
            var resArr = decodedString.Split(',');
            ViewBag.UserName = resArr[0].ToString();
            bool isExpired = Convert.ToDateTime(resArr[1].ToString()) > DateTime.Now ? false : true;
            ViewBag.IsExpired = isExpired;
            ViewBag.IsUser = false;

            var isUser = false;

            if (!isExpired)
            {
                var res1 = _service.Get<object>(_global.ApiUrl() + "/api/v1/user/GetUserByUserName?UserName=" + resArr[0].ToString());

                var model = JsonConvert.DeserializeObject<UserResponseTracker>((String)res1.Item3);
                isUser = model.Users.FirstOrDefault().UserID > 0 ? true : false;
                ViewBag.IsUser = isUser;
            }


            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(string Email, String Password)
        {

            var jSon = new JavaScriptSerializer().Serialize(new RegistrationModel() { EmailID = Email, Password = Password });
            var res = _service.Post<CommonResponseDataModel>(_global.ApiUrl() + "/api/v1/user/updateUserPassword", jSon);
            if (res.Item3.ReturnID > 0)
            {

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.IsError = res.Item3.Message;

            }
            return View();
        }
        public static void Email(string userName)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("deba.somu@gmail.com");
                message.To.Add(new MailAddress(userName));
                message.Subject = "Test";
                message.IsBodyHtml = false; //to make message body as html  
                message.Body = "";
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com"; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("deba.somu@gmail.com", "9002276670");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception) { }
        }
    }
}