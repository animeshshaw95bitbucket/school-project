﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.Interface
{
    public interface IGlobal
    {
        /// <summary>
        /// DefaultPageListSize
        /// </summary>
        /// <returns></returns>
        Int32 DefaultPageListSize();
        /// <summary>
        /// ApiUrl
        /// </summary>
        /// <returns></returns>
        String ApiUrl();
        /// <summary>
        /// AudienceID
        /// </summary>
        /// <returns></returns>
        String AudienceID();
        /// <summary>
        /// Issuer
        /// </summary>
        /// <returns></returns>
        String Issuer();
        /// <summary>
        /// SignKey
        /// </summary>
        /// <returns></returns>
        String SignKey();
        /// <summary>
        /// AuthTokenExpiry
        /// </summary>
        /// <returns></returns>
        String AuthTokenExpiry();
        /// <summary>
        /// GeneralErrorMessage
        /// </summary>
        /// <returns></returns>
        String GeneralErrorMessage();

        /// <summary>
        /// GeneralErrorMessage
        /// </summary>
        /// <returns></returns>
        Int32 DefaultAPITimeout();

    }
}
