﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Core.ViewModel;

namespace Web.Core.Interface
{
    public interface ILogin
    {
        LoginResponseViewModel Login(LoginRequestViewModel request);
    }
}
