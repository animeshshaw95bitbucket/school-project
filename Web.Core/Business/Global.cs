﻿using System;
using Web.Core.Interface;

namespace Web.Core.Business
{
    public sealed class Global : IGlobal
    {
        private readonly Int32 _defaultpagelistsize;
        private readonly String _apiurl;
        public readonly String _audienceID;
        public readonly String _issuer;
        public readonly String _signKey;
        public readonly String _authTokenExpiry;
        public readonly String _generalerrormessage;
        public readonly String _defaultapitimeout;

        public Global(Int32 defaultpagelistsize, String apiurl, String authTokenExpiry,String audienceID, String issuer, String signKey , String generalerrormessage, String defaultapitimeout)
        {
            _defaultpagelistsize = defaultpagelistsize;
            _apiurl = apiurl;
            _audienceID = audienceID;
            _issuer = issuer;
            _signKey = signKey;
            _authTokenExpiry = authTokenExpiry;
            _generalerrormessage = generalerrormessage;
            _defaultapitimeout = defaultapitimeout;
        }

        public Int32 DefaultPageListSize()
        {
            return _defaultpagelistsize;
        }

        public String ApiUrl()
        {
            return _apiurl;
        }
        public String AudienceID()
        {
            return _audienceID;
        }
        public String Issuer()
        {
            return _issuer;
        }
        public String SignKey()
        {
            return _signKey;
        }
        public String AuthTokenExpiry()
        {
            return _authTokenExpiry;
        }
        public String GeneralErrorMessage()
        {
            return _generalerrormessage;
        }
        public Int32 DefaultAPITimeout()
        {
            return Convert.ToInt32(_defaultapitimeout);
        }
    }
}
