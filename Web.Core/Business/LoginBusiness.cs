﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Web.Core.APIModel;
using Web.Core.Common;
using Web.Core.Interface;
using Web.Core.ViewModel;

namespace Web.Core.Business
{
    public class LoginBusiness : ILogin
    {
        IGlobal _global;
        CommonServices _commonServices;
        #region CTOR
        public LoginBusiness(IGlobal global)
        {
            _global = global;
            _commonServices = new CommonServices(global);
        }
        #endregion
        #region Constants
        private readonly String LoginURL="/Token";
        #endregion
        public LoginResponseViewModel Login(LoginRequestViewModel request)
        {
            LoginResponseViewModel resp = new LoginResponseViewModel();
            try
            {
                string URL = _global.ApiUrl() + LoginURL;
                using (WebClient client = new WebClient())
                {
                    var reqparm = new System.Collections.Specialized.NameValueCollection();
                    reqparm.Add("grant_type", "password");
                    reqparm.Add("username", request.UserName);
                    reqparm.Add("password", request.Password);
                    byte[] responsebytes = client.UploadValues(URL, "POST", reqparm);
                    string token = Encoding.UTF8.GetString(responsebytes);
                    TokenResponse objtoken = JsonConvert.DeserializeObject<TokenResponse>(token);
                    token = objtoken.access_token;

                    var principals = _commonServices.ValidateJwtToken(token);
                    if(principals!=null && principals.Claims!=null)
                    {
                        string userDetails = principals.Claims.FirstOrDefault(c => c.Type == "UserDetails").Value;
                        resp = JsonConvert.DeserializeObject<LoginResponseViewModel>(userDetails);
                        resp.Token = token;
                        resp.Successfull = true;
                        resp.claims = principals.Claims.ToList();
                    }

                }

            }
            catch (Exception ex)
            {
                resp.Successfull = false;
                resp.Message = _global.GeneralErrorMessage();
            }
            return resp;

        }
    }
}
