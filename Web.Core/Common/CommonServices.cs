﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Web.Core.Interface;

namespace Web.Core.Common
{
    public class CommonServices
    {
        IGlobal _global;
        #region CTOR
        public CommonServices(IGlobal global)
        {
            _global = global;
        }
        #endregion
        public Tuple<HttpStatusCode, List<Parameter>, T> Get<T>(string APIUrl)
        {
            Tuple<HttpStatusCode, List<Parameter>, T> _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.BadRequest, null, Activator.CreateInstance<T>());
            var Headers = GetDefaultHeaders();
            try
            {
                GC.Collect();
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                var client = new RestClient(APIUrl);
                client.Timeout = _global.DefaultAPITimeout();
                var request = new RestRequest(Method.GET);
                foreach (var header in Headers)
                    request.AddHeader(header.Key, header.Value);
                IRestResponse response = client.Execute(request);
                var responseHeader = response.Headers.ToList();
                T responseClass = JsonConvert.DeserializeObject<T>(response.Content);
                _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(response.StatusCode, responseHeader, responseClass);
            }
            catch (WebException wex)
            {
                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = wex.Response as IRestResponse;
                    if (response != null)
                    {
                        _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(response.StatusCode, response.Headers.ToList(), Activator.CreateInstance<T>());
                    }
                    else
                    {
                        _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.InternalServerError, null, Activator.CreateInstance<T>());
                        // no http status code available
                    }
                }
                else
                {
                    _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.InternalServerError, null, Activator.CreateInstance<T>());
                    // no http status code available
                }

            }
            catch (Exception ex)
            {
                _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.InternalServerError, null, Activator.CreateInstance<T>());
            }
            return _resp;
        }

       
        public Tuple<HttpStatusCode, List<Parameter>, T> Post<T>(string APIUrl, string Body)
        {
            Tuple<HttpStatusCode, List<Parameter>, T> _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.BadRequest, null, Activator.CreateInstance<T>());
            var Headers = GetDefaultHeaders();
            try
            {
                GC.Collect();
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                var client = new RestClient(APIUrl);
                client.Timeout = _global.DefaultAPITimeout();
                var request = new RestRequest(Method.POST);
                foreach (var header in Headers)
                    request.AddHeader(header.Key, header.Value);

                request.AddParameter("application/json", Body, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                var responseHeader = response.Headers.ToList();
                T responseClass = JsonConvert.DeserializeObject<T>(response.Content);
                _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(response.StatusCode, responseHeader, responseClass);

            }
            catch (WebException wex)
            {
                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = wex.Response as IRestResponse;
                    if (response != null)
                    {
                        _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(response.StatusCode, response.Headers.ToList(), Activator.CreateInstance<T>());
                    }
                    else
                    {
                        _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.InternalServerError, null, Activator.CreateInstance<T>());
                        // no http status code available
                    }
                }
                else
                {
                    _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.InternalServerError, null, Activator.CreateInstance<T>());
                    // no http status code available
                }
            }
            catch (Exception ex)
            {
                _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.InternalServerError, null, Activator.CreateInstance<T>());
            }
            return _resp;
        }

        
        public Tuple<HttpStatusCode, List<Parameter>, T> Delete<T>(string APIUrl, Dictionary<string, string> Headers)
        {
            Tuple<HttpStatusCode, List<Parameter>, T> _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.BadRequest, null, Activator.CreateInstance<T>());
            try
            {
                GC.Collect();
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                var client = new RestClient(APIUrl);
                client.Timeout = _global.DefaultAPITimeout();
                var request = new RestRequest(Method.DELETE);
                foreach (var header in Headers)
                    request.AddHeader(header.Key, header.Value);
                IRestResponse response = client.Execute(request);
                var responseHeader = response.Headers.ToList();
                T responseClass = JsonConvert.DeserializeObject<T>(response.Content);
                _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(response.StatusCode, responseHeader, responseClass);
            }
            catch (WebException wex)
            {
                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = wex.Response as IRestResponse;
                    if (response != null)
                    {
                        _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(response.StatusCode, response.Headers.ToList(), Activator.CreateInstance<T>());
                    }
                    else
                    {
                        _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.InternalServerError, null, Activator.CreateInstance<T>());
                        // no http status code available
                    }
                }
                else
                {
                    _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.InternalServerError, null, Activator.CreateInstance<T>());
                    // no http status code available
                }

            }
            catch (Exception ex)
            {
                _resp = new Tuple<HttpStatusCode, List<Parameter>, T>(HttpStatusCode.InternalServerError, null, Activator.CreateInstance<T>());
            }
            return _resp;
        }

        public Dictionary<string, string> GetDefaultHeaders()
        {
            Dictionary<string, string> resp = new Dictionary<string, string>();
            resp.Add("Content-Type", "application/json");
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["UserAuthToken"])))
                    resp.Add("Authorization", "Bearer " + Convert.ToString(HttpContext.Current.Session["UserAuthToken"]));
            }
            catch (Exception ex) { }
            return resp;
        }
        public ClaimsPrincipal ValidateJwtToken(string jwtToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var credential = Convert.FromBase64String(_global.SignKey());
            var sSKey = new InMemorySymmetricSecurityKey(credential);

            // Parse JWT from the Base64UrlEncoded wire form (<Base64UrlEncoded header>.<Base64UrlEncoded body>.<signature>)
            JwtSecurityToken parsedJwt = tokenHandler.ReadToken(jwtToken) as JwtSecurityToken;

            TokenValidationParameters validationParams =
                new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidAudience = _global.AudienceID(),
                    ValidIssuer = _global.Issuer(), // TokenIssuer,
                    ValidateAudience = true,
                    ValidateIssuer = true,
                    IssuerSigningKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(credential)
                };
            try
            {
                Microsoft.IdentityModel.Tokens.SecurityToken validatedToken;
                var principal = tokenHandler.ValidateToken(jwtToken, validationParams, out validatedToken);
                return principal;
            }
            catch (Exception e)
            {
            }
            return null;


        }

    }
}
