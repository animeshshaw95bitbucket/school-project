﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
   public class SectionDataModel
    {
        public int SectionID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public string SectionCode { get; set; }
        public string SectionName { get; set; }
        public string SectionDescription { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
