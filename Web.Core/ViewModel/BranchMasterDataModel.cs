﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
   public class BranchMasterDataModel
    {
        public int BranchID { get; set; }
        public string BranchName { get; set; }
        public int BranchTypeID { get; set; }
        public string BranchTypeName { get; set; }
        public string BranchCode { get; set; }
        public string BranchAddress { get; set; }
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }
        public string Vill { get; set; }
        public string PostOffice { get; set; }
        public string Block { get; set; }
        public string GP_Municipality { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool IsActive { get; set; }
    }
}

 

