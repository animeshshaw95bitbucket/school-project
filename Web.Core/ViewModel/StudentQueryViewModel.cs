﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
    public class StudentQueryViewModel
    {
        public int StudentQueryID { get; set; }
        public String StudentName { get; set; }
        public String FathersName { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }
        public Int32 GenderID { get; set; }
        public String Gender { get; set; }
        public int BranchID { get; set; }
        public String BranchName { get; set; }
        public String StateId { get; set; }
        public String DistrictId { get; set; }
        public String StateName { get; set; }
        public String DistrictName { get; set; }
        public String Location { get; set; }
        public String PinCode { get; set; }
        public string Query { get; set; }
        public string Comment { get; set; }
        public bool IsRead { get; set; }
        public bool IsRespond { get; set; }
        public int Status { get; set; }
        public bool IsActive { get; set; }
        public Int32? UpdatedBy { get; set; }
        public Int32? CreatedBy { get; set; }
        public int TotalReadCount { get; set; }
        public int TotalUnreadCount { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
