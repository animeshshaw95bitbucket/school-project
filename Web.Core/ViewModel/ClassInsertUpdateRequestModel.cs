﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Core.ViewModel
{
    public class ClassInsertUpdateRequestModel
    {
        public int ClassID { get; set; }
        [Required(ErrorMessage = "Class name required")]
        public string ClassName { get; set; }
        public string ClassDescription { get; set; }
        [Required(ErrorMessage = "Session required")]
        public int SessionID { get; set; }
        public List<SessionMasterDataModel> ddlSessions { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
    }
}
