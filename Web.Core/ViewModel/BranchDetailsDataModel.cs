﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
   public class BranchDetailsDataModel
    {
        public BranchMasterDataModel Branch { get; set; }
        public List<BranchOwnerDataModel> BranchOwners { get; set; }
    }
}
