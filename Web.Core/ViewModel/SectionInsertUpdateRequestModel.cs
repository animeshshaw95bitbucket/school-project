﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
    public class SectionInsertUpdateRequestModel
    {
        public int SectionID { get; set; }
        [Required(ErrorMessage = "Section name required")]
        public string SectionName { get; set; }
        public string SectionDesc { get; set; }
        [Required(ErrorMessage = "Class required")]
        public int ClassID { get; set; }
        public List<ClassMasterDataModel> ddlClass { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
    }
}
