﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
    public class DistrictDataModel
    {
        public int StateID { get; set; }
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }
        public string StateCode { get; set; }
    }
}
