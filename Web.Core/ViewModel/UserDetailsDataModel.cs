﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
  public class UserDetailsDataModel : CommonResponseDataModel
    {
        public int UserID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String EmailID { get; set; }
        public String Password { get; set; }
        public String PhoneNumber { get; set; }
        public List<RoleDataModel> UserRoles { get; set; }
        public String ProfileImageUrl { get; set; }
    }
}
