﻿using System.ComponentModel.DataAnnotations;

namespace Web.Core.ViewModel
{
    public class SessionInsertUpdateRequestModel
    {
        public int SessionID { get; set; }
        [Required(ErrorMessage = "Session name required")]
        public string SessionName { get; set; }
        public string SessionDesc { get; set; }
        public string PrincipalSignaturePath { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
    }
}
