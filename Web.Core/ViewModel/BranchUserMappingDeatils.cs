﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
   public class BranchUserMappingDeatils
    {
        public List<BranchMasterDataModel> Branch { get; set; }
        public List<BranchUserDataModel> BranchUsers { get; set; }
    }
}
