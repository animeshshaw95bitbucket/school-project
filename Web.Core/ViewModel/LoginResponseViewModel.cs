﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
    public class LoginResponseViewModel
    {
        public int UserID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String EmailID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<DateTime> LastLoginTime { get; set; }
        public bool Successfull { get; set; }
        public String Message { get; set; }
        public string Token { get; set; }
        public List<Claim> claims { get; set; }
    }
}
