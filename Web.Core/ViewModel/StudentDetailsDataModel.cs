﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
    public class StudentDetailsDataModel
    {
        public long StudentID { get; set; }
        public string StudentCode { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateOfAdmission { get; set; }
        public int BloodGroup { get; set; }
        public int Nationality { get; set; }
        public string AdmissionCode { get; set; }
        public int Caste { get; set; }
        public int SubCaste { get; set; }
        public string CertificateNumber { get; set; }
        public string CertificateIssueBox { get; set; }
        public string Religion { get; set; }
        public int IsPhysicallyHandicap { get; set; }
        public int PhysicallyHandicapPercentage { get; set; }
        public string PhysicallyHandicapDescription { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
