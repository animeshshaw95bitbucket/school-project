﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{

    public class BranchDetailsInsertUpdateRequestModel
    {
        public int BranchID { get; set; }
        [Required(ErrorMessage = "BranchName Required")]
        public String BranchName { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "BranchTypeID Should be greater than 1")]
        [Required(ErrorMessage = "BranchTypeID Required")]
        public int BranchTypeID { get; set; }

       // [Required(ErrorMessage = "BranchCode Required")]
        public String BranchCode { get; set; }

        // [Required(ErrorMessage = "BranchAddress Required")]
        public String BranchAddress { get; set; } = "TEST";
        [Range(1, Int32.MaxValue, ErrorMessage = "StateID Should be greater than 1")]
        [Required(ErrorMessage = "StateID Required")]
        public int StateID { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "DistrictID Should be greater than 1")]
        [Required(ErrorMessage = "DistrictID Required")]
        public int DistrictID { get; set; }
        [Required(ErrorMessage = "Village Required")]
        public String Vill { get; set; }
        [Required(ErrorMessage = "PostOffice Required")]
        public String PostOffice { get; set; }
        [Required(ErrorMessage = "Block Required")]
        public String Block { get; set; }
        [Required(ErrorMessage = "GP_Municipality Required")]
        public String GP_Municipality { get; set; }
        public bool IsActive { get; set; } = true;
        public List<BranchTypeDataModel> BranchTypeList { get; set; }
        public List<StateDataModel> StateList { get; set; }
        public List<DistrictDataModel> DistrictList { get; set; }
        public List<BranchCreateOwnerRequestModel> Owners { get; set; }
    }
}

