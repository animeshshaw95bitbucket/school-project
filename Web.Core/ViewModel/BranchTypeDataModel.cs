﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
 
    public class BranchTypeDataModel
    {
        public int BranchTypeId { get; set; }
        public string BranchTypeName { get; set; }
    }
}
