﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
   
    public class StateDataModel
    {
        public int StateID { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
    }
}
