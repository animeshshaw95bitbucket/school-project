﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
    public class UserRoleDataModel
    {
        public int UserID { get; set; }
        public int MappingID { get; set; }
        public int RoleID { get; set; }

    }
}
