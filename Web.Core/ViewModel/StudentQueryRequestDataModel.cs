﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
    public class StudentQueryRequestDataModel
    {
        public Int32? StudentQueryID { get; set; }
        public Int32? BranchId { get; set; }
        public String StudentName { get; set; }
        public String BranchName { get; set; }
        public Boolean? IsRead { get; set; }
        public Boolean? IsRespond { get; set; }
        public Int32? Status { get; set; }
        public String SearchText { get; set; }
    }
}
