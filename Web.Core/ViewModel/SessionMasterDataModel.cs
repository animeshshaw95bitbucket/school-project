﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Web.Core.ViewModel
{
    public class SessionMasterDataModel
    {
        public int SessionID { get; set; }
        public string SessionName { get; set; }
        public string SessionDescription { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string PrincipalSignature { get; set; }
    }
}
