﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
    public class BranchQueryViewModel
    {
        public int BranchQueryId { get; set; }
        public String EdupreneurName { get; set; }
        public String EdupreneurAddress { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }
        public int BranchTypeId { get; set; }
        public int StateId { get; set; }
        public int DistrictId { get; set; }
        public String PostOfficeName { get; set; }
        public String PoliceStationName { get; set; }
        public String Location { get; set; }
        public String PinCode { get; set; }
        public string Query { get; set; }
        public string Comment { get; set; }
        public bool IsRead { get; set; }
        public bool IsRespond { get; set; }
        public String UpdatedBy { get; set; }
        public int TotalReadCount { get; set; }
        public int TotalUnreadCount { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
