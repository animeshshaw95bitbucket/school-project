﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
    public class LoginRequestViewModel
    {
        [Required(ErrorMessage = "UserName required")]
        public String UserName { get; set; }
        [Required(ErrorMessage ="Password required")]
        public String Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
