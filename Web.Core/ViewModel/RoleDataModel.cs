﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Core.ViewModel
{
   public class RoleDataModel
    {
        public int RoleID { get; set; }
        public String RoleName { get; set; }
    }
}
