﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;


namespace WebAPI.Data.DBContext
{
    public sealed class PArray
    {
        public String PName { get; set; }
        public Object PValue { get; set; }
        public SqlDbType PType { get; set; }
    }

    public sealed class DataConnection
    {
        #region Stored Procedure Execute Methods With Data Reader

        /// <summary>
        /// Get Data Objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <returns></returns>
        public IEnumerable<T> GetDataObjects<T>(DbDataReader reader) where T : class, new()
        {
            var list = new List<T>();
            try
            {
                if (reader == null)
                    return list;

                HashSet<string> tableColumnNames = null;
                while (reader.Read())
                {
                    tableColumnNames = tableColumnNames ?? CollectColumnNames(reader);
                    var entity = new T();
                    foreach (var propertyInfo in typeof(T).GetProperties())
                    {
                        object retrievedObject = null;
                        if (tableColumnNames.Contains(propertyInfo.Name))
                        {
                            if (reader[propertyInfo.Name] != System.DBNull.Value && reader[propertyInfo.Name] != null)
                            {
                                retrievedObject = reader[propertyInfo.Name];
                                propertyInfo.SetValue(entity, retrievedObject);
                            }
                        }
                    }
                    list.Add(entity);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return list;
        }

        /// <summary>
        /// Collect Column Names
        /// </summary>
        /// <param name="Reader"></param>
        /// <returns></returns>
        static HashSet<string> CollectColumnNames(DbDataReader Reader)
        {
            var _CollectedColumnInfo = new HashSet<string>();
            for (int i = 0; i < Reader.FieldCount; i++)
            {
                _CollectedColumnInfo.Add(Reader.GetName(i));
            }
            return _CollectedColumnInfo;
        }

        /// <summary>
        /// Execute Stored Procedure For Reader
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="Con"></param>
        /// <param name="Parameters"></param>
        /// <returns></returns>
        public DbDataReader ExecuteStoredProcedureForReader(string commandText, string Con, params object[] Parameters)
        {
            var _Connection = new SqlConnection(Con);
            if (_Connection.State == ConnectionState.Closed)
                _Connection.Open();
            ////var _transaction = _Connection.BeginTransaction();
            try
            {
                using (var _Cmd = _Connection.CreateCommand())
                {
                    _Cmd.CommandText = commandText;
                    _Cmd.CommandType = CommandType.StoredProcedure;
                    _Cmd.CommandTimeout = 12500000;

                    string exec = EXECGetSPString(commandText, Parameters);

                    if (Parameters != null)
                        foreach (var p in Parameters)
                        {
                            if (p != null)
                                _Cmd.Parameters.Add(p);
                        }
                    var _Reader = _Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    ////_transaction.Commit();
                    return _Reader;
                }
            }
            catch (Exception ex)
            {
                Console.Error.Write(ex.Message);
                throw;
            }

        }

        public static string EXECGetSPString(string spname, params object[] arr)
        {
            string temp = "Exec " + spname;
            for (int i = 0; i < arr.Length; i++)
            {
                temp += " " + arr[i] + "  = '" + ((System.Data.SqlClient.SqlParameter)arr[i]).Value + "' , " + Environment.NewLine;
            }
            temp = temp.TrimEnd(new char[] { ',' });
            return temp;
        }


        #endregion

        #region Store Procedure Execute Methods With DataSet and DataTable

        /// -----------------------------------------------------------------------------
        /// <title>makePArray</title>
        /// <summary>Function create an arraylist of SQL parameters.</summary>
        /// <param name="Parameter">The name of the parameter.</param>
        /// <param name="PValue">The value of the paramter.</param>
        /// <param name="SQLType">The SQL Data Input Type.</param>
        /// <example>
        /// <code>
        /// Dim DB as new ClassLibrary.Library.DBConn
        /// Dim ID as integer
        /// Dim myArray As New ArrayList
        /// myArray.add(DB.makePArray("@i_intID", ID, SqlDbType.Int))
        /// </code>
        /// </example>
        /// <returns>Arraylist</returns>
        /// <history>
        /// 	Sujoy	12/05/2011	Created
        /// </history>
        /// -----------------------------------------------------------------------------

        public PArray makePArray(String Parameter, Object PValue, SqlDbType SQLType)
        {
            PArray parameters = new PArray();
            parameters.PName = Parameter;
            parameters.PValue = PValue;
            parameters.PType = SQLType;
            return parameters;
        }

        /// -----------------------------------------------------------------------------
        /// <title>getData</title>
        /// <summary>Function returning to return a SQL dataset.</summary>
        /// <param name="SPName">The Stored Procedure name.</param>
        /// <param name="myPArray">An arraylist of input parameters and values.</param>
        /// <param name="myConn">The SQL database connection string.</param>
        /// <returns>Dataset</returns>
        /// <history>
        /// 	Sujay	Created
        /// </history>
        /// -----------------------------------------------------------------------------

        public DataSet getData(String SPName, List<PArray> myPArray, String myConn)
        {
            DataSet DS = new DataSet();
            SqlConnection myConnection = new SqlConnection(myConn);
            if (myConnection.State == ConnectionState.Closed)
                myConnection.Open();
            /////var _transaction = myConnection.BeginTransaction();

            SqlDataAdapter myCommand = new SqlDataAdapter(SPName, myConnection);
            try
            {
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                if (myPArray != null)
                {
                    foreach (PArray parameter in myPArray)
                    {
                        using (var cmd = myCommand.SelectCommand)
                        {
                            cmd.Parameters.Add(new SqlParameter(parameter.PName, parameter.PType));
                            cmd.Parameters[parameter.PName].Value = RenderNull(parameter.PValue, parameter.PType);
                        }
                    }
                }
                myCommand.Fill(DS, SPName);
                /////_transaction.Commit();
            }
            catch (Exception ex)
            {
                /////_transaction.Rollback();
                throw;
            }
            finally
            {
                myPArray = null;
                if (myConnection.State == ConnectionState.Open)
                    myConnection.Close();
                myCommand.Dispose();
                myConnection.Dispose();
            }
            return DS;
        }

        /// -----------------------------------------------------------------------------
        /// <title>GetDataTable</title>
        /// <summary>Function returning to return a SQL dataset.</summary>
        /// <param name="SPName">The Stored Procedure name.</param>
        /// <param name="myPArray">An arraylist of input parameters and values.</param>
        /// <param name="TableID">The index of the table to retrieve data from.</param>
        /// <param name="myConn">The SQL database connection string.</param>
        /// <returns>DataTable</returns>
        /// <history>
        /// 	Sujay	Created
        /// </history>
        /// -----------------------------------------------------------------------------

        public DataTable GetDataTable(String SPName, List<PArray> myPArray, Int32 TableID, String Conn)
        {
            DataSet DS = new DataSet();

            try
            {
                DS = getData(SPName, myPArray, Conn);
                DataTable dt = DS.Tables[TableID];
                return dt;
            }
            catch { return null; }
            finally { DS.Dispose(); }
        }

        /// <title>GetSQLDBType</title>
        /// <summary>Function to return the correct SQLDBType for the given input.</summary>
        /// <param name="Obj">The Stored Procedure Parameter Name.</param>
        /// <returns>SqlDbType</returns>
        /// <history>
        /// 	Sujay	Created
        /// </history>
        /// -----------------------------------------------------------------------------

        public SqlDbType GetSQLDBType(String ParameterName)
        {
            SqlDbType SQLType = SqlDbType.VarChar;

            if (ParameterName.Contains("@i_bin"))
                SQLType = SqlDbType.VarBinary;
            else if (ParameterName.Contains("@i_bit"))
                SQLType = SqlDbType.Bit;
            else if (ParameterName.Contains("@i_dce"))
                SQLType = SqlDbType.Decimal;
            else if (ParameterName.Contains("@i_dte"))
                SQLType = SqlDbType.DateTime;
            else if (ParameterName.Contains("@i_int"))
                SQLType = SqlDbType.Int;
            else if (ParameterName.Contains("@i_mny"))
                SQLType = SqlDbType.Money;
            else if (ParameterName.Contains("@i_dt"))
                SQLType = SqlDbType.DateTime;
            else if (ParameterName.Contains("@i_bint"))
                SQLType = SqlDbType.BigInt;

            return SQLType;
        }

        /// <title>GetSQLDBType</title>
        /// <summary>Function to return the correct SQLDBType for the given input.</summary>
        /// <param name="Obj">The Stored Procedure Parameter Name.</param>
        /// <returns>SqlDbType</returns>
        /// <history>
        /// 	Sujay	Created
        /// </history>
        /// -----------------------------------------------------------------------------

        public List<PArray> Add(String ParameterName, Object Value)
        {
            PArray newPArray = new PArray();
            newPArray.PName = ParameterName;
            newPArray.PValue = Value;
            newPArray.PType = GetSQLDBType(ParameterName);
            PList.Add(newPArray);
            return PList;
        }



        public List<PArray> PList { get; set; }

        /// <title>RenderNull</title>
        /// <summary>Function render nulls to the database.</summary>
        /// <param name="Obj">The Stored Procedure object.</param>
        /// <returns>Object</returns>
        /// <history>
        /// 	Sujay	Created
        /// </history>
        /// -----------------------------------------------------------------------------

        public Object RenderNull(Object Obj, SqlDbType SQLType)
        {
            if (SQLType == SqlDbType.DateTime)
            {
                if (Convert.ToDateTime(Obj) == DateTime.MinValue)
                    Obj = null;
            }

            if (Convert.IsDBNull(Obj) || Obj == null)
                return System.DBNull.Value;
            else
                return Obj;
        }

        /// <title>ConvertNull</title>
        /// <summary>Function to convert nulls for specific input types.</summary>
        /// <param name="input">The input value.</param>
        /// <returns>Object</returns>
        /// <history>
        /// 	Sujay	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public Object ConvertNull(Object input)
        {
            if (Convert.IsDBNull(input) == true)
            {
                if (input.GetType() == typeof(Boolean))
                    return false;
                else if (input.GetType() == typeof(Int32))
                    return Int32.MinValue;
                else if (input.GetType() == typeof(Double))
                    return Double.MinValue;
                else if (input.GetType() == typeof(DateTime))
                    return DateTime.MinValue;
                else
                    return null;
            }
            else
                return input;
        }

        /// -----------------------------------------------------------------------------
        /// <title>ConvertDataRowToModel</title>
        /// <summary>Function to render DataRow to a new instance of a Model.</summary>
        /// <param name="row">The input DataRow.</param>
        /// <param name="T">The input Model type.</param>
        /// <returns>The filled input Model.</returns>
        /// <history>
        /// 	Sujay	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public T ConvertDataRowToModel<T>(DataRow row) where T : new()
        {
            // create a new object
            T item = new T();

            // set the item
            SetItemFromRow(item, row);

            // return 
            return item;
        }

        /// -----------------------------------------------------------------------------
        /// <title>SetItemFromRow</title>
        /// <summary>Function to convert DataRow columns to the properties in a Model.</summary>
        /// <param name="item">A new instance of the input Model type (T).</param>
        /// <param name="row">The input DataRow.</param>
        /// <returns>Void - renders DataRow columns to the new instance of the input Model.</returns>
        /// <history>
        /// 	Sujay	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public void SetItemFromRow<T>(T item, DataRow row) where T : new()
        {
            // go through each column
            foreach (DataColumn c in row.Table.Columns)
            {
                // find the property for the column
                PropertyInfo p = item.GetType().GetProperty(c.ColumnName);

                // if exists, set the value
                if (p != null && row[c] != DBNull.Value)
                {
                    p.SetValue(item, row[c], null);
                }
            }
        }

        /// -----------------------------------------------------------------------------
        /// <title>ConvertDataTable</title>
        /// <summary>Function to convert a DataTable to enumerables of a Model (T).</summary>
        /// <param name="datatable">The input DataTable value.</param>
        /// <param name="T">The Model type to convert to.</param>
        /// <returns>Enumerables of the input Model (T).</returns>
        /// <history>
        /// 	Sujay	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public IEnumerable<T> ConvertDataTable<T>(DataTable datatable) where T : new()
        {
            if (datatable != null)
            {
                foreach (DataRow row in datatable.Rows)
                    yield return ConvertDataRowToModel<T>(row);
            }
            else
                yield break;
        }

        /// -----------------------------------------------------------------------------
        /// <title>ConvertDataRow</title>
        /// <summary>Function to convert a DataRow to a single instance of a Model.</summary>
        /// <param name="row">The input DataRow.</param>
        /// <param name="T">The Model type to convert to.</param>
        /// <returns>A single instance of the input Model (T).</returns>
        /// <history>
        /// 	Sujay	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public T ConvertDataRow<T>(DataRow row) where T : new()
        {
            if (row != null)
                return ConvertDataRowToModel<T>(row);
            else
                return default(T);

        }

        public DataRow GetReturnError(String errormessage)
        {
            // Here we create a DataTable with four columns.
            DataTable table = new DataTable();
            table.Columns.Add("ReturnID", typeof(Int32));
            table.Columns.Add("Message", typeof(String));
            table.Columns.Add("IsSuccessful", typeof(Boolean));

            // Here we add five DataRows.
            table.Rows.Add(null, errormessage, false);

            return table.Rows[0];
        }


        public DataSet ExecuteStoredProcedureForDataset(string commandText, string Con, params object[] Parameters)
        {
            DataSet DS = new DataSet();
            SqlConnection myConnection = new SqlConnection(Con);
            if (myConnection.State == ConnectionState.Closed)
                myConnection.Open();
            /////var _transaction = myConnection.BeginTransaction();

            SqlDataAdapter myCommand = new SqlDataAdapter(commandText, myConnection);
            try
            {
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (Parameters != null)
                    foreach (var parameter in Parameters)
                    {
                        if (parameter != null)
                            using (var cmd = myCommand.SelectCommand)
                            {
                                cmd.Parameters.Add(parameter);
                            }
                    }
                myCommand.Fill(DS, commandText);

            }
            catch (Exception ex)
            {
                Console.Error.Write(ex.Message);
                throw;
            }
            finally
            {
                Parameters = null;
                if (myConnection.State == ConnectionState.Open)
                    myConnection.Close();
                myCommand.Dispose();
                myConnection.Dispose();
            }
            return DS;

        }
        #endregion
    }

    public sealed class SqlServerParamManager
    {
        IDataParameter _param;

        public SqlServerParamManager()
        {
            // _dataProvider = provider;
        }

        /// <summary>
        /// Gets Parameter for Sproc with default parameter name "Id"
        /// </summary>
        /// <param name="value">parameter object value</param>
        /// <returns></returns>
        public IDataParameter Get(object value)
        {
            return Get("Id", value);
        }

        /// <summary>
        /// Gets Parameter for Sproc with default parameter direction as input & dbtype as string
        /// </summary>
        /// <param name="paramName"> parameter name w/o @</param>
        /// <param name="value">parameter object value</param>
        /// <returns></returns>
        public IDataParameter Get(string paramName, object value)
        {
            return Get(paramName, value, ParameterDirection.Input, DbType.String);
        }

        /// <summary>
        ///  Gets Parameter for Sproc with default paramete dbtype as string
        /// </summary>
        /// <param name="paramName">parameter name w/o @</param>
        /// <param name="value">parameter object value</param>
        /// <param name="direction">parameter direction as input/output</param>
        /// <returns></returns>
        public IDataParameter Get(string paramName, object value, ParameterDirection direction)
        {
            return Get(paramName, value, direction, DbType.String);
        }

        /// <summary>
        ///  Gets Parameter for Sproc
        /// </summary>
        /// <param name="paramName">parameter name w/o @</param>
        /// <param name="value">parameter object value</param>
        /// <param name="direction">parameter direction as input/output</param>
        /// <param name="type">parameter datatype</param>
        /// <returns></returns>
        public IDataParameter Get(string paramName, object value, ParameterDirection direction, DbType type)
        {
            // _param = _dataProvider.GetParameter();
            _param = new SqlParameter();
            _param.ParameterName = paramName;
            _param.Value = value;
            _param.Direction = direction;
            _param.DbType = type;
            return _param as SqlParameter;
        }

        /// <summary>
        /// Gets Parameter for Sproc
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="value"></param>
        /// <param name="direction"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IDataParameter GetNew(string paramName, object value, ParameterDirection direction, SqlDbType type)
        {
            SqlParameter _param = new SqlParameter();
            _param.ParameterName = paramName;
            _param.Value = value;
            _param.Direction = direction;
            _param.SqlDbType = type;
            return _param as SqlParameter;
        }

        /// <summary>
        /// Gets Parameter for Sproc
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="direction"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IDataParameter GetNew(string paramName, ParameterDirection direction, SqlDbType type)
        {
            SqlParameter _param = new SqlParameter();
            _param.ParameterName = paramName;
            _param.Direction = direction;
            _param.SqlDbType = type;
            return _param as SqlParameter;
        }

        /// <summary>
        /// Gets Parameter for Sproc
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="direction"></param>
        /// <param name="type"></param>
        /// <param name="Size"></param>
        /// <returns></returns>
        public IDataParameter GetNew(string paramName, ParameterDirection direction, SqlDbType type, int Size)
        {
            SqlParameter _param = new SqlParameter();
            _param.ParameterName = paramName;
            _param.Direction = direction;
            _param.SqlDbType = type;
            _param.Size = Size;
            return _param as SqlParameter;
        }
    }
}
