﻿using WebAPI.Core.Code;
using System;
using System.Configuration;

namespace WebAPI.Data.Code
{
    public sealed class Global : IGlobal
    {
        private readonly String _connectionstring;
        private readonly Int32 _defaultpagelistsize;
        private readonly String _websitename;
        private readonly String _websiteurl;
        public readonly String _fileuploadpath;
        public readonly String _frontendwebsiteurl;
        public readonly String _fileuploadrawpath;
        public readonly String _IOSCert;
        public readonly String _error;
        public readonly String _tempFolderPath;
        public readonly String _frontEndWebFolderPath;
        public readonly String _generalErrorMessgae;
        public readonly String _audienceID;
        public readonly String _issuer;
        public readonly String _signKey;
        public readonly String _authTokenExpiry;
        private static String DBConn = ConfigurationManager.AppSettings["WebAPIPlatformDBConn"];

        public Global()
        {


            _connectionstring = ConfigurationManager.ConnectionStrings[DBConn].ConnectionString;
            _defaultpagelistsize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageListSize"]); ;
            _websitename = ConfigurationManager.AppSettings["WebsiteName"];
            _websiteurl = ConfigurationManager.AppSettings["WebsiteURL"];
            _fileuploadpath = ConfigurationManager.AppSettings["FilePath"]; ;
            _frontendwebsiteurl = ConfigurationManager.AppSettings["FrontEndWebsiteURL"]; ;
            //_fileuploadrawpath = fileuploadrawpath;
            _IOSCert = ConfigurationManager.AppSettings["IOSCert"]; ;
            _error = ConfigurationManager.AppSettings["Error"];
            _tempFolderPath = ConfigurationManager.AppSettings["TempFolderPath"]; ;
            _frontEndWebFolderPath = ConfigurationManager.AppSettings["FrontEndWebFolderPath"]; ;
            _generalErrorMessgae = "An error occurred.";
            _audienceID = ConfigurationManager.AppSettings["audienceID"]; ;
            _issuer = ConfigurationManager.AppSettings["issuer"]; ;
            _signKey = ConfigurationManager.AppSettings["signKey"]; ;
            _authTokenExpiry = ConfigurationManager.AppSettings["AuthTokenExpiry"]; ;
        }
        public Global(String connectionstring, Int32 defaultpagelistsize, String websitename, String websiteurl,String authTokenExpiry,
            String fileuploadpath, String frontendwebsiteurl, String fileuploadrawpath, String IOSCert, String error,
            String tempFolderPath, String frontEndWebFolderPath, String generalErrorMessgae, String audienceID, String issuer, String signKey)
        {
            _connectionstring = connectionstring;
            _defaultpagelistsize = defaultpagelistsize;
            _websitename = websitename;
            _websiteurl = websiteurl;
            _fileuploadpath = fileuploadpath;
            _frontendwebsiteurl = frontendwebsiteurl;
            _fileuploadrawpath = fileuploadrawpath;
            _IOSCert = IOSCert;
            _error = error;
            _tempFolderPath = tempFolderPath;
            _frontEndWebFolderPath = frontEndWebFolderPath;
            _generalErrorMessgae = generalErrorMessgae;
            _audienceID = audienceID;
            _issuer = issuer;
            _signKey = signKey;
            _authTokenExpiry = authTokenExpiry;
        }

        public String ConnectionString()
        {
            return _connectionstring;
        }

        public Int32 DefaultPageListSize()
        {
            return _defaultpagelistsize;
        }

        public String WebsiteName()
        {
            return _websitename;
        }

        public String WebsiteUrl()
        {
            return _websiteurl;
        }
        public String FileUploadPath()
        {
            return _fileuploadpath;
        }

        public String FrontEndWebsiteURL()
        {
            return _frontendwebsiteurl;
        }

        public String FileUploadRawPath()
        {
            return _fileuploadrawpath;
        }

        public String IOSCert()
        {
            return _IOSCert;
        }

        public String Error()
        {
            return _error;
        }
        public String TempFolderPath()
        {
            return _tempFolderPath;
        }

        public String FrontEndWebFolderPath()
        {
            return _frontEndWebFolderPath;
        }

        public String GeneralErrorMessgae()
        {
            return _generalErrorMessgae;
        }
        public String AudienceID()
        {
            return _audienceID;
        }
        public String Issuer()
        {
            return _issuer;
        }
        public String SignKey()
        {
            return _signKey;
        }
        public String AuthTokenExpiry()
        {
            return _authTokenExpiry;
        }
    }
}
