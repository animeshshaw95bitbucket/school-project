﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBAccess;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.Code
{
    public class SectionBusiness : ISection
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly SectionDBAccesssModel SectionDBA;
        private readonly IGlobal _global;
        #endregion

        #region CTOR
        public SectionBusiness(IGlobal global)
        {
            _global = global;
            SectionDBA = new SectionDBAccesssModel(global);
            DB = new DataConnection();
        }
        #endregion

        #region Public methods
        public IEnumerable<SectionDataModel> GetSection(int? sectionID = null, string sectionName = null, string sectionCode = null)
        {
            List<SectionDataModel> datalist = new List<SectionDataModel>();
            var dataSet = SectionDBA.GetSectionDetails(sectionName, sectionID, sectionCode);
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                datalist = JsonConvert.DeserializeObject<List<SectionDataModel>>(JsonConvert.SerializeObject(dataSet.Tables[0]));
            }
            return datalist;
        }
        public CommonResponseDataModel InsertUpdateSection(SectionRequestModel request)
        {
            CommonResponseDataModel datalist;
            var _DataReader = SectionDBA.InsertUpdateSection(request);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }

        public CommonResponseDataModel UpdateStatusSection(StatusRequestModel request)
        {
            CommonResponseDataModel datalist;
            var _DataReader = SectionDBA.UpdateStatusSection(request);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }
        #endregion
    }
}
