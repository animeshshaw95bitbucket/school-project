﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBAccess;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.Code
{
    public class SessionBusiness:ISession
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly SessionDBAccessModel SessionDBA;
        private readonly IGlobal _global;
        #endregion

        #region CTOR
        public SessionBusiness(IGlobal global)
        {
            _global = global;
            SessionDBA = new SessionDBAccessModel(global);
            DB = new DataConnection();
        }
        #endregion

        #region Public methods
        public IEnumerable<SessionMaster> GetSession(int? sessionID = null, string sessionName = null, string classCode = null)
        {
            List<SessionMaster> datalist = new List<SessionMaster>();
            var dataSet = SessionDBA.GetSessionDetails(sessionName, sessionID, classCode);
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                datalist = JsonConvert.DeserializeObject<List<SessionMaster>>(JsonConvert.SerializeObject(dataSet.Tables[0]));
            }
            return datalist;
        }

        public CommonResponseDataModel InsertUpdateSession(SessionRequestModel request)
        {
            CommonResponseDataModel datalist;
            var _DataReader = SessionDBA.InsertUpdateSession(request);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }

        public CommonResponseDataModel UpdateStatusSession(StatusRequestModel request)
        {
            CommonResponseDataModel datalist;
            var _DataReader = SessionDBA.UpdateStatusSession(request);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }
        #endregion

    }
}
