﻿using WebAPI.Core;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBAccess;
using WebAPI.Data.DBContext;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Data.Code
{
    public sealed class AuthenticateBusiness : IAuthenticate
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly AuthenticateDBAccessModel ADAM;
        //CommonHelper objCom = new CommonHelper();
        #endregion

        #region CTOR
        public AuthenticateBusiness(IGlobal _global)
        {
            DB = new DataConnection();
            ADAM = new AuthenticateDBAccessModel(_global);
        }
        #endregion
        public AuthenticateDataModels UserLogin(AuthenticateRequestModel requestModel)
        {
            IEnumerable<AuthenticateDataModels> datalist;
            var _DataReader = ADAM.UserLogin(requestModel);
            datalist = DB.GetDataObjects<AuthenticateDataModels>(_DataReader);
            return datalist.FirstOrDefault();
        }
      

    }

}