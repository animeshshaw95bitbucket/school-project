﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBAccess;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.Code
{
    public class StudentBusiness : IStudent
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly StudentDBAccessModel StudentDBA;
        private readonly IGlobal _global;
        #endregion

        #region CTOR
        public StudentBusiness(IGlobal global)
        {
            _global = global;
            StudentDBA = new StudentDBAccessModel(global);
            DB = new DataConnection();
        }
        #endregion

        #region Student Query
        public CommonResponseDataModel AddUpdateStudentQuery(AddUpdateStudentQueryRequest request)
        {
            CommonResponseDataModel datalist;
            var _DataReader = StudentDBA.AddUpdateStudentQuery(request);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }

        public CommonResponseDataModel UpdateIsReadStudentQuery(int StudentQueryID, bool isRead)
        {
            CommonResponseDataModel datalist;
            var _DataReader = StudentDBA.UpdateIsReadStudentQuery(StudentQueryID, isRead);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }
        public IEnumerable<StudentQueryResponseDataModel> GetStudentQuery(SearchStudentQueryRequest request)
        {
            List<StudentQueryResponseDataModel> datalist = new List<StudentQueryResponseDataModel>(); ;
            var dataSet = StudentDBA.GetStudentQuery(request);
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                datalist = JsonConvert.DeserializeObject<List<StudentQueryResponseDataModel>>(JsonConvert.SerializeObject(dataSet.Tables[0]));
            }
            return datalist;
        }
        #endregion
    }
}
