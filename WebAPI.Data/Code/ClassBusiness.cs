﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBAccess;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.Code
{
    public class ClassBusiness : IClass
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly ClassDBAccessModel ClassDBA;
        private readonly IGlobal _global;
        #endregion

        #region CTOR
        public ClassBusiness(IGlobal global)
        {
            _global = global;
            ClassDBA = new ClassDBAccessModel(global);
            DB = new DataConnection();
        }
        #endregion

        #region Public methods
        public IEnumerable<ClassDataModel> GetClass(int? classID = null, string className = null)
        {
            List<ClassDataModel> datalist = new List<ClassDataModel>();
            var dataSet = ClassDBA.GetClassDetails(className, classID);
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                datalist = JsonConvert.DeserializeObject<List<ClassDataModel>>(JsonConvert.SerializeObject(dataSet.Tables[0]));
            }
            return datalist;
        }

        public CommonResponseDataModel InsertUpdateClass(ClassRequestModel request)
        {
            CommonResponseDataModel datalist;
            var _DataReader = ClassDBA.InsertUpdateClass(request);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }

        public CommonResponseDataModel UpdateStatusClass(StatusRequestModel request)
        {
            CommonResponseDataModel datalist;
            var _DataReader = ClassDBA.UpdateStatusClass(request);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }
        #endregion
    }
}
