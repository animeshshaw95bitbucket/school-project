﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Data.DBAccess;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.Code
{
    public class RoleBusiness :IRole
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly RoleDBAccessModel RDAM;
        //CommonHelper objCom = new CommonHelper();
        #endregion

        #region CTOR
        public RoleBusiness(IGlobal _global)
        {
            DB = new DataConnection();
            RDAM = new RoleDBAccessModel(_global);
        }
        #endregion
        public IEnumerable<RoleDataModel> GetAllRoles()
        {
            IEnumerable<RoleDataModel> datalist;
            var _DataReader = RDAM.GetAllRoles();
            datalist = DB.GetDataObjects<RoleDataModel>(_DataReader);
            return datalist;
        }

        public IEnumerable<RoleDataModel> GetUserRoles(int UserID)
        {
            IEnumerable<RoleDataModel> datalist;
            var _DataReader = RDAM.GetUserRoles(UserID);
            datalist = DB.GetDataObjects<RoleDataModel>(_DataReader);
            return datalist;
        }
    }
}
