﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBAccess;
using WebAPI.Data.DBContext;


namespace WebAPI.Data.Code
{
   public class HeadOfficeMasterBusiness : IHeadOfficeMaster
    {

        #region Global Variable
        private readonly DataConnection DB;
        private readonly HeadOfficeDBAccessModel HeadOfficeDBA;
       // private readonly CommonHelper objCommonHelper;
        #endregion

        #region CTOR
        public HeadOfficeMasterBusiness(IGlobal _global)
        {
            DB = new DataConnection();
            HeadOfficeDBA = new HeadOfficeDBAccessModel(_global);
           // objCommonHelper = new CommonHelper();
        }
        #endregion

        #region Get
        /// <summary>
        /// Get Head Office Master
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public IEnumerable<HeadOfficeDataModel> GetHeadOffice(HeadOfficeGetRequestModel requestModel)
        {
            return PV_GetHeadOfficeMaster(requestModel);
        }

        private IEnumerable<HeadOfficeDataModel> PV_GetHeadOfficeMaster(HeadOfficeGetRequestModel requestData)
        {
            IEnumerable<HeadOfficeDataModel> datalist;
            var _DataReader = HeadOfficeDBA.GetHeadOfficeMasters(requestData);
            datalist = DB.GetDataObjects<HeadOfficeDataModel>(_DataReader);
            return datalist;
        }


        #endregion

        #region Insert

        /// <summary>
        /// Insert Client
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public CommonResponseDataModel InsertHeadOfficeMaster(HeadOfficeCreateRequestModel requestData)
        {
            return PV_InsertHeadOffice(requestData);
        }

        private CommonResponseDataModel PV_InsertHeadOffice(HeadOfficeCreateRequestModel requestData)
        {
            var _DataReader = HeadOfficeDBA.InsertUpdateHeadOffice(requestData);
            var obj = DB.GetDataObjects<CommonResponseDataModel>(_DataReader);
           
            return obj.FirstOrDefault();
        }

        #endregion


        //#region Update

        ///// <summary>
        ///// Update Client
        ///// </summary>
        ///// <param name="requestData"></param>
        ///// <returns></returns>
        //public CommonResponseDataModel UpdateClient(ClientUpdateRequestModel requestData)
        //{
        //    return PV_UpdateClient(requestData);
        //}

        //private CommonResponseDataModel PV_UpdateClient(ClientUpdateRequestModel requestData)
        //{
        //    if (objCommonHelper.GetLongValue(requestData.ClientID) == 0)
        //    {
        //        var UpdateResponse = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.BadRequest), ReturnID = 0, Message = "Please provide the Client id", Successful = false };
        //        return UpdateResponse;
        //    }
        //    else
        //    {
        //        var _DataReader = ClientDBA.UpdateClient(requestData);
        //        var obj = DB.GetDataObjects<CommonResponseDataModel>(_DataReader);
        //        if (!_DataReader.IsClosed) { _DataReader.Close(); }
        //        if (obj.Any())
        //        {
        //            if (objCommonHelper.GetLongValue(obj.ElementAt(0).ReturnID) > 0 && objCommonHelper.GetStringValue(obj.ElementAt(0).Message) != "")
        //            {
        //                var UpdateResponse = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.Ok), ReturnID = objCommonHelper.GetLongValue(obj.ElementAt(0).ReturnID), Message = objCommonHelper.GetStringValue(obj.ElementAt(0).Message), Successful = obj.ElementAt(0).Successful, IsAuthorized = objCommonHelper.GetBooleanValue(obj.ElementAt(0).IsAuthorized) };
        //                return UpdateResponse;
        //            }
        //            else
        //            {
        //                if (objCommonHelper.GetStringValue(obj.ElementAt(0).Message) != "")
        //                {
        //                    var Response = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.NotFound), ReturnID = objCommonHelper.GetLongValue(0), Message = objCommonHelper.GetStringValue(obj.ElementAt(0).Message), Successful = false, IsAuthorized = objCommonHelper.GetBooleanValue(obj.ElementAt(0).IsAuthorized) };
        //                    return Response;
        //                }
        //                else
        //                {
        //                    var Response = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.NotFound), ReturnID = objCommonHelper.GetLongValue(0), Message = "Client cannot be updated. Please enter correct data", Successful = false, IsAuthorized = objCommonHelper.GetBooleanValue(obj.ElementAt(0).IsAuthorized) };
        //                    return Response;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            var UpdateResponse = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.Ok), ReturnID = objCommonHelper.GetLongValue(obj.ElementAt(0).ReturnID), Message = "Client cannot be updated", Successful = true, IsAuthorized = objCommonHelper.GetBooleanValue(obj.ElementAt(0).IsAuthorized) };
        //            return UpdateResponse;
        //        }
        //    }
        //}

        //#endregion

        //#region Delete

        ///// <summary>
        ///// Delete Client
        ///// </summary>
        ///// <param name="requestData"></param>
        ///// <returns></returns>
        //public CommonResponseDataModel DeleteClient(ClientDeleteRequestModel requestData)
        //{
        //    return PV_DeleteClient(requestData);
        //}

        //private CommonResponseDataModel PV_DeleteClient(ClientDeleteRequestModel requestData)
        //{
        //    if (objCommonHelper.GetLongValue(requestData.ClientID) == 0)
        //    {
        //        var UpdateResponse = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.BadRequest), ReturnID = 0, Message = "Please provide the Client id", Successful = false };
        //        return UpdateResponse;
        //    }
        //    else
        //    {
        //        var _DataReader = ClientDBA.DeleteClient(requestData);
        //        var obj = DB.GetDataObjects<CommonResponseDataModel>(_DataReader);
        //        if (!_DataReader.IsClosed) { _DataReader.Close(); }
        //        if (obj.Any())
        //        {
        //            if (objCommonHelper.GetBooleanValue(obj.ElementAt(0).Successful))
        //            {
        //                var UpdateResponse = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.Ok), ReturnID = objCommonHelper.GetLongValue(obj.ElementAt(0).ReturnID), Message = objCommonHelper.GetStringValue(obj.ElementAt(0).Message), Successful = true, IsAuthorized = objCommonHelper.GetBooleanValue(obj.ElementAt(0).IsAuthorized) };
        //                return UpdateResponse;
        //            }
        //            else
        //            {
        //                var UpdateResponse = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.NotModified), ReturnID = objCommonHelper.GetLongValue(obj.ElementAt(0).ReturnID), Message = objCommonHelper.GetStringValue(obj.ElementAt(0).Message), Successful = false, IsAuthorized = objCommonHelper.GetBooleanValue(obj.ElementAt(0).IsAuthorized) };
        //                return UpdateResponse;
        //            }
        //        }
        //        else
        //        {
        //            var UpdateResponse = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.Ok), ReturnID = objCommonHelper.GetLongValue(obj.ElementAt(0).ReturnID), Message = "Client cannot be deleted", Successful = true };
        //            return UpdateResponse;
        //        }
        //    }
        //}

        //#endregion


        //#region Update Active in active for Client

        ///// <summary>
        ///// Update Active in active for Client
        ///// </summary>
        ///// <param name="clientID"></param>
        ///// <param name="requestData"></param>
        ///// <returns></returns>
        //public CommonResponseDataModel UpdateActiveinActiveClient(Int64 clientID, ClientActiveInActiveRequestModel requestData)
        //{
        //    return PV_UpdateActiveinActiveClient(clientID, requestData);
        //}

        //private CommonResponseDataModel PV_UpdateActiveinActiveClient(Int64 clientID, ClientActiveInActiveRequestModel requestData)
        //{
        //    var _DataReader = ClientDBA.UpdateActiveInActiveClient(clientID, requestData);
        //    var obj = DB.GetDataObjects<CommonResponseDataModel>(_DataReader);
        //    if (!_DataReader.IsClosed) { _DataReader.Close(); }
        //    if (obj.Any())
        //    {
        //        var UpdateResponse = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.Ok), ReturnID = objCommonHelper.GetLongValue(obj.ElementAt(0).ReturnID), Message = objCommonHelper.GetStringValue(obj.ElementAt(0).Message), Successful = true };
        //        return UpdateResponse;
        //    }
        //    else
        //    {
        //        var UpdateResponse = new CommonResponseDataModel { Code = Convert.ToInt32(StatusCode.Ok), ReturnID = objCommonHelper.GetLongValue(obj.ElementAt(0).ReturnID), Message = "Client cannot be updated", Successful = true };
        //        return UpdateResponse;
        //    }
        //}

        //#endregion


    }
}
