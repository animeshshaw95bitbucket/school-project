﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBAccess;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.Code
{
    public class BranchBusiness : IBranch
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly BranchDBAccessModel BranchDBA;
        private readonly IGlobal _global;
        #endregion

        #region CTOR
        public BranchBusiness(IGlobal global)
        {
            _global = global;
            BranchDBA = new BranchDBAccessModel(global);
            DB = new DataConnection();
        }
        #endregion
        public IEnumerable<BranchDetails> GetBranchDetails(String branchName = null, int? BranchID = null, int? BranchOwnerID = null, int? BranchTypeID = null, int? StateID = null, int? DistrictID = null)
        {
            List<BranchDetails> datalist = new List<BranchDetails>(); ;
            var dataSet = BranchDBA.GetBranchDetails(branchName, BranchID, BranchOwnerID, BranchTypeID,StateID,DistrictID);
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                List<BranchMaster> branchMasters = JsonConvert.DeserializeObject<List<BranchMaster>>(JsonConvert.SerializeObject(dataSet.Tables[0]));
                List<BranchOwner> branchOwners = JsonConvert.DeserializeObject<List<BranchOwner>>(JsonConvert.SerializeObject(dataSet.Tables[1]));
                foreach (var bm in branchMasters)
                {
                    BranchDetails branchDetails = new BranchDetails()
                    {
                        Branch = bm,
                        BranchOwners = branchOwners.Where(x => x.BranchID == bm.BranchID).ToList()
                    };
                    datalist.Add(branchDetails);
                }
            }

            return datalist;
        }
        public IEnumerable<BranchQueryResponseDataModel> GetBranchQueryDetails(int? branchQueryID)
        {
            List<BranchQueryResponseDataModel> datalist = new List<BranchQueryResponseDataModel>(); ;
            var dataSet = BranchDBA.GetBranchQueryDetails(branchQueryID);
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                datalist = JsonConvert.DeserializeObject<List<BranchQueryResponseDataModel>>(JsonConvert.SerializeObject(dataSet.Tables[0]));

            }

            return datalist;
        }
        public IEnumerable<BranchMaster> GetBranchListDDL()
        {
            List<BranchMaster> datalist = new List<BranchMaster>(); ;
            var dataSet = BranchDBA.GetBranchDetails();
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                datalist = JsonConvert.DeserializeObject<List<BranchMaster>>(JsonConvert.SerializeObject(dataSet.Tables[0]));

            }

            return datalist;
        }
        public CommonResponseDataModel InsertUpdateBranch(BranchCreateRequestModel request)
        {
            CommonResponseDataModel datalist;
            var _DataReader = BranchDBA.InsertUpdateBranch(request);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }
        public CommonResponseDataModel InsertUpdateBranchOwner(List<BranchOwnerCreateRequestModel> request)
        {
            CommonResponseDataModel datalist;
            string xml = CreateBranchOwnerXML(request);
            var _DataReader = BranchDBA.InsertUpdateBranchOwner(xml);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }
        private String CreateBranchOwnerXML(List<BranchOwnerCreateRequestModel> request)
        {
            string s = "<Owners>";
            foreach (var r in request)
            {
                var doj = r.DOJ == DateTime.MinValue ? "" : r.DOJ.Date.ToString("yyyy-MM-dd");
                var dob = r.DOB == DateTime.MinValue ? "" : r.DOB.Date.ToString("yyyy-MM-dd");
                r.SecondaryContactNo = r.SecondaryContactNo ?? r.PrimaryContactNo; 
                string tmp = "<Owner>";
                tmp += "<BranchOwnerID>" + r.BranchOwnerID.ToString() + "</BranchOwnerID>";
                tmp += "<BranchID>" + r.BranchID.ToString() + "</BranchID>";
                tmp += "<OwnerName>" + r.OwnerName.ToString() + "</OwnerName>";
                tmp += "<QualificationID>" + r.QualificationID.ToString() + "</QualificationID>";
                tmp += "<Designation>" + r.Designation.ToString() + "</Designation>";
                tmp += "<DOB>" + dob + "</DOB>";
                tmp += "<DOJ>" + doj  + "</DOJ>";
                tmp += "<AadharNo>" + r.AadharNo.ToString() + "</AadharNo>";
                tmp += "<VoterNo>" + r.VoterNo.ToString() + "</VoterNo>";
                tmp += "<PanNo>" + r.PanNo.ToString() + "</PanNo>";
                tmp += "<PrimaryContactNo>" + r.PrimaryContactNo.ToString() + "</PrimaryContactNo>";
                tmp += "<SecondaryContactNo>" + r.SecondaryContactNo + "</SecondaryContactNo>";
                tmp += "<EmailAddress>" + r.EmailAddress.ToString() + "</EmailAddress>";
                if (r.IsActive)
                    tmp += "<IsActive>" + "1" + "</IsActive>";
                else
                    tmp += "<IsActive>" + "0" + "</IsActive>";
                tmp += "</Owner>";
                s += tmp;
            }
            s += "</Owners>";
            return s;
        }

        public CommonResponseDataModel InsertUpdateBranchUser(BranchUserCreateRequestModel request)
        {
            CommonResponseDataModel datalist;
            var _DataReader = BranchDBA.InsertUpdateBranchUser(request);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }
        public CommonResponseDataModel InsertUpdateBranchQuery(CreateBranchQueryRequest request)
        {
            CommonResponseDataModel datalist;
            var _DataReader = BranchDBA.InsertUpdateBranchQuery(request);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }

        public CommonResponseDataModel UpdateIsReadBranchQuery(int BranchQueryID, bool isRead)
        {
            CommonResponseDataModel datalist;
            var _DataReader = BranchDBA.UpdateIsReadBranchQuery(BranchQueryID,isRead);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();
            return datalist;
        }

        public IEnumerable<BranchTypes> GetBranchTypes()
        {

            var _DataReader = BranchDBA.GetBranchType();
            var datalist = DB.GetDataObjects<BranchTypes>(_DataReader);
            return datalist;

        }

        public IEnumerable<StateList> GetAllState()
        {

            var _DataReader = BranchDBA.GetAllState();
            var datalist = DB.GetDataObjects<StateList>(_DataReader);
            return datalist;

        }
        public IEnumerable<DistrictDataModel> GetAllDistrict(int? stateId = null)
        {

            var _DataReader = BranchDBA.GetAllDistrict(stateId);
            var datalist = DB.GetDataObjects<DistrictDataModel>(_DataReader);
            return datalist;

        }

        public BranchUsersDetails GetBranchUsers(int? BranchID)
        {
            BranchUsersDetails dataList = new BranchUsersDetails();

            var _DataReader = BranchDBA.GetBranchUsers(BranchID);
            dataList.Branch = DB.GetDataObjects<BranchMaster>(_DataReader) ?? new List<BranchMaster>();
            _DataReader.NextResult();
            dataList.BranchUsers = DB.GetDataObjects<UserDataModel>(_DataReader) ?? new List<UserDataModel>();
            return dataList;
        }
    }
}
