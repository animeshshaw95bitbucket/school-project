﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBAccess;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.Code
{
    public class UserBusiness : IUser
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly UserDBAccessModel RDAM;
        //CommonHelper objCom = new CommonHelper();
        #endregion

        #region CTOR
        public UserBusiness(IGlobal _global)
        {
            DB = new DataConnection();
            RDAM = new UserDBAccessModel(_global);
        }
        #endregion
        public CommonResponseDataModel AddUser(AddUserRequestModel model)
        {
            CommonResponseDataModel datalist;
            var _DataReader = RDAM.AddUser(model);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();

            return datalist;
        }
        public CommonResponseDataModel UpdateUserPassword(AddUserRequestModel model)
        {
            CommonResponseDataModel datalist;
            var _DataReader = RDAM.UpdateUserPassword(model);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();

            return datalist;
        }
        public UserDetailsResponseModel GetUsersDetails(int? UserID = null, int? BranchId = null, string UserName = null)
        {
            UserDetailsResponseModel datalist = new UserDetailsResponseModel();
            var _DataReader = RDAM.GetUsersDetails(UserID, BranchId, UserName);
            datalist.Users = DB.GetDataObjects<UserDataModel>(_DataReader) ?? new List<UserDataModel>();
            _DataReader.NextResult();
            datalist.Roles = DB.GetDataObjects<RoleDataModel>(_DataReader) ?? new List<RoleDataModel>();
            _DataReader.NextResult();
            datalist.UserRole = DB.GetDataObjects<UserRoleMappingModel>(_DataReader) ?? new List<UserRoleMappingModel>();

            return datalist;
        }

        public UserDetailsResponseModel GetUserByUserName(string userName)
        {
            UserDetailsResponseModel datalist = new UserDetailsResponseModel();
            var _DataReader = RDAM.GetUserByUserName(userName);
            datalist.Users = DB.GetDataObjects<UserDataModel>(_DataReader) ?? new List<UserDataModel>();
            datalist.Roles = new List<RoleDataModel>();
            datalist.UserRole = new List<UserRoleMappingModel>();


            return datalist;

        }
        public CommonResponseDataModel InsertUpdteUserRole(int UserID, string RoleIds)
        {

            var _DataReader = RDAM.InsertUpdteUserRole(UserID, RoleIds);
            var res = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();

            return res;
        }

        public UserDataModel GetUserProfile(int? UserID = null, string UserName = null)
        {
            UserDataModel output = new UserDataModel();
            var _DataReader = RDAM.GetUsersDetails(UserID, null, UserName);
            var datalist = DB.GetDataObjects<UserDataModel>(_DataReader) ?? new List<UserDataModel>();

            if (datalist != null && datalist.Any())
                output = datalist.FirstOrDefault();

            return output;
        }

        public CommonResponseDataModel UpdateProfile(UpdateProfileRequestModel model)
        {
            CommonResponseDataModel datalist;
            var _DataReader = RDAM.UpdateProfile(model);
            datalist = DB.GetDataObjects<CommonResponseDataModel>(_DataReader).FirstOrDefault();

            return datalist;
        }
    }
}
