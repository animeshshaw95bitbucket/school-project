﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.DBAccess
{
    public class SessionDBAccessModel
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly IGlobal Global;
        private readonly SqlServerParamManager _parameterManager;
        // private readonly JSONConverter _JSONConverter;
        #endregion

        #region CTOR
        public SessionDBAccessModel(IGlobal global)
        {
            Global = global;
            DB = new DataConnection();
            _parameterManager = new SqlServerParamManager();
        }
        #endregion

        #region Constents
        private readonly String SessionID = "@SessionID";
        private readonly String SessionName = "@SessionName";
        private readonly String SessionDescription = "@SessionDescription";
        private readonly String PrincipalSignature = "@PrincipalSignature";
        private readonly String IsActive = "@IsActive";
        private readonly String CreatedBy = "@CreatedBy";
        #endregion

        #region public functions
        public DataSet GetSessionDetails(String sessionName = null, int? sessionID = null,string classCode=null)
        {
            try
            {
                List<PArray> _params = new List<PArray>()
                {
                    new PArray()
                    {
                        PName=SessionID,
                        PType=SqlDbType.Int,
                        PValue=sessionID ?? SqlInt32.Null
                    },
                    new PArray()
                    {
                        PName=SessionName,
                        PType=SqlDbType.NVarChar,
                        PValue=sessionName ?? SqlString.Null
                    }
                };
                return DB.getData("usp_GetSessionDetails", _params
                                    , Global.ConnectionString().ToString()
                                );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader InsertUpdateSession(SessionRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUpdateSessionMaster", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.SessionID, request.SessionID)
                                                            , _parameterManager.Get(this.SessionName, request.SessionName)
                                                            , _parameterManager.Get(this.SessionDescription, request.SessionDesc)
                                                            , _parameterManager.Get(this.PrincipalSignature, request.PrincipalSignaturePath)
                                                            , _parameterManager.Get(this.IsActive, request.IsActive)
                                                            , _parameterManager.Get(this.CreatedBy, request.CreatedBy)
                                                            
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader UpdateStatusSession(StatusRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_UpdateStatusSession", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.SessionID, request.ID)
                                                            , _parameterManager.Get(this.IsActive, request.IsActive)
                                                            , _parameterManager.Get(this.CreatedBy, request.CreatedBy)

                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
