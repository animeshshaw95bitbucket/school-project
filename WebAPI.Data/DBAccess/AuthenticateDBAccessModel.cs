﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.DBAccess
{
    public sealed class AuthenticateDBAccessModel
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly IGlobal Global;
        private readonly SqlServerParamManager _parameterManager;
        #endregion

        #region CTOR
        public AuthenticateDBAccessModel(IGlobal global)
        {
            Global = global;
            DB = new DataConnection();
            _parameterManager = new SqlServerParamManager();
        }
        #endregion
        #region Constants
        private readonly String EmailID = "@EmailID";
        private readonly String Password = "@Password";
        #endregion

        #region Public Methods
        public DbDataReader UserLogin(AuthenticateRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_ValidateUser", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(EmailID, request.UserName),
                                                            _parameterManager.Get(Password, request.Password)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
