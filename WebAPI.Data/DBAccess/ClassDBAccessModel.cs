﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.DBAccess
{
    public class ClassDBAccessModel
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly IGlobal Global;
        private readonly SqlServerParamManager _parameterManager;
        // private readonly JSONConverter _JSONConverter;
        #endregion

        #region CTOR
        public ClassDBAccessModel(IGlobal global)
        {
            Global = global;
            DB = new DataConnection();
            _parameterManager = new SqlServerParamManager();
        }
        #endregion

        #region Constents
        private readonly String ClassID = "@ClassID";
        private readonly String ClassName = "@ClassName";
        private readonly String ClassDescription = "@ClassDescription";
        private readonly String SessionID = "@SessionID";
        private readonly String IsActive = "@IsActive";
        private readonly String CreatedBy = "@CreatedBy";
        #endregion

        #region Public functions
        public DataSet GetClassDetails(String className = null, int? classID = null)
        {
            try
            {
                List<PArray> _params = new List<PArray>()
                {
                    new PArray()
                    {
                        PName=ClassID,
                        PType=SqlDbType.Int,
                        PValue=classID ?? SqlInt32.Null
                    },
                    new PArray()
                    {
                        PName=ClassName,
                        PType=SqlDbType.NVarChar,
                        PValue=className ?? SqlString.Null
                    }
                };
                return DB.getData("usp_GetClassDetails", _params
                                    , Global.ConnectionString().ToString()
                                );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader InsertUpdateClass(ClassRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUpdateClass", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.ClassID, request.ClassID)
                                                            , _parameterManager.Get(this.ClassName, request.ClassName)
                                                            , _parameterManager.Get(this.ClassDescription, request.ClassDescription)
                                                            , _parameterManager.Get(this.SessionID, request.SessionID)
                                                            , _parameterManager.Get(this.IsActive, request.IsActive)
                                                            , _parameterManager.Get(this.CreatedBy, request.CreatedBy)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader UpdateStatusClass(StatusRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_UpdateStatusClass", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.ClassID, request.ID)
                                                            , _parameterManager.Get(this.IsActive, request.IsActive)
                                                            , _parameterManager.Get(this.CreatedBy, request.CreatedBy)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
