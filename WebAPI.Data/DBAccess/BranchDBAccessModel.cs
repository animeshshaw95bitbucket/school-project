﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.DBAccess
{
    
    public class BranchDBAccessModel
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly IGlobal Global;
        private readonly SqlServerParamManager _parameterManager;
        // private readonly JSONConverter _JSONConverter;
        #endregion

        #region CTOR
        public BranchDBAccessModel(IGlobal global)
        {
            Global = global;
            DB = new DataConnection();
            _parameterManager = new SqlServerParamManager();
        }
        #endregion

        #region Constants
        private readonly String BranchID = "@BranchID";
        private readonly String BranchOwnerID = "@BranchOwnerID";
        private readonly String BranchTypeID = "@BranchTypeID";
        private readonly String BranchName = "@BranchName";
        private readonly String BranchCode = "@BranchCode";
        private readonly String BranchAddress = "@BranchAddress";
        private readonly String StateID = "@StateID";
        private readonly String DistrictID = "@DistrictID";
        private readonly String Vill = "@Vill";
        private readonly String PostOffice = "@PostOffice";
        private readonly String Block = "@Block";
        private readonly String GP_Municipality = "@GP_Municipality";
        private readonly String IsActive = "@IsActive";
        private readonly String mxlStr = "@mxlStr";
        private readonly String StateCode = "@StateCode";
        private readonly String UserID = "@UserID";
        private readonly String BranchTypeId = "@BranchTypeId";
        private readonly String BranchQueryId = "@BranchQueryId";
        private readonly String IsRead = "@IsRead";
        private readonly String UpdatedBy = "@UpdatedBy";
        private readonly String EdupreneurName = "@EdupreneurName";
        private readonly String EdupreneurAddress = "@EdupreneurAddress";
        private readonly String Phone = "@Phone";
        private readonly String Email = "@Email";
        private readonly String PostOfficeName = "@PostOfficeName";
        private readonly String PoliceStationName = "@PoliceStationName";
        private readonly String Location = "@Location";
        private readonly String PinCode = "@PinCode";
        private readonly String Query = "@Query";
        private readonly String Comment = "@Comment";
       // private readonly String StateID = "@StateID";
       // private readonly String StateID = "@StateID";
        #endregion

        /// <summary>
        /// Get Branch Details
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DataSet GetBranchDetails(String branchName = null, int? branchID = null, int? branchOwnerID = null, int? branchTypeID = null, int? stateID = null, int? districtID = null)
        {
            try
            {
                List<PArray> _params = new List<PArray>()
                {
                    new PArray()
                    {
                        PName=BranchID,
                        PType=SqlDbType.Int,
                        PValue=branchID ?? SqlInt32.Null
                    },
                    new PArray()
                    {
                        PName=BranchOwnerID,
                        PType=SqlDbType.Int,
                        PValue=branchOwnerID ?? SqlInt32.Null
                    },
                    new PArray()
                    {
                        PName=BranchTypeID,
                        PType=SqlDbType.Int,
                        PValue=branchTypeID ?? SqlInt32.Null
                    },
                    new PArray()
                    {
                        PName=BranchName,
                        PType=SqlDbType.NVarChar,
                        PValue=branchName ?? SqlString.Null
                    },
                     new PArray()
                    {
                        PName=StateID,
                        PType=SqlDbType.Int,
                        PValue=stateID  ?? SqlInt32.Null
                    },
                      new PArray()
                    {
                        PName=DistrictID,
                        PType=SqlDbType.Int,
                        PValue=districtID  ?? SqlInt32.Null
                    }
                };
                return DB.getData("usp_GetBranchDetails", _params
                                    , Global.ConnectionString().ToString()
                                );

            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet GetBranchListDDL()
        {
            try
            {
                List<PArray> _params = new List<PArray>()
                {
                    new PArray()
                    {
                        PName=BranchID,
                        PType=SqlDbType.Int,
                        PValue= SqlInt32.Null
                    },
                    new PArray()
                    {
                        PName=BranchOwnerID,
                        PType=SqlDbType.Int,
                        PValue= SqlInt32.Null
                    },
                    new PArray()
                    {
                        PName=BranchTypeID,
                        PType=SqlDbType.Int,
                        PValue= SqlInt32.Null
                    }
                };
                return DB.getData("usp_GetBranchDetails", _params
                                    , Global.ConnectionString().ToString()
                                );

            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet GetBranchQueryDetails(int? branchQueryID)
        {
            try
            {
                List<PArray> _params = new List<PArray>()
                {
                    new PArray()
                    {
                        PName=BranchTypeId,
                        PType=SqlDbType.Int,
                        PValue= SqlInt32.Null
                    },
                    new PArray()
                    {
                        PName=BranchQueryId,
                        PType=SqlDbType.Int,
                        PValue= branchQueryID.HasValue ? branchQueryID.Value : (object)null
                    }
                };
                return DB.getData("usp_GetBranchQuery", _params
                                    , Global.ConnectionString().ToString()
                                );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader InsertUpdateBranch(BranchCreateRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUpdateBranchMaster", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.BranchID, request.BranchID)
                                                            , _parameterManager.Get(this.BranchName, request.BranchName)
                                                            , _parameterManager.Get(this.BranchTypeID, request.BranchTypeID)
                                                            , _parameterManager.Get(this.BranchCode, request.BranchCode)
                                                            , _parameterManager.Get(this.BranchAddress, request.BranchAddress)
                                                            , _parameterManager.Get(this.StateID, request.StateID)
                                                            , _parameterManager.Get(this.DistrictID, request.DistrictID)
                                                            , _parameterManager.Get(this.Vill, request.Vill)
                                                            , _parameterManager.Get(this.PostOffice, request.PostOffice)
                                                            , _parameterManager.Get(this.Block, request.Block)
                                                            , _parameterManager.Get(this.GP_Municipality, request.GP_Municipality)
                                                            , _parameterManager.Get(this.IsActive, request.IsActive)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader InsertUpdateBranchUser(BranchUserCreateRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUpdateBranchUsers", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.BranchID, request.BranchID)
                                                            , _parameterManager.Get(this.UserID, request.UserID)
                                                            , _parameterManager.Get(this.IsActive, request.IsActive)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        public DbDataReader InsertUpdateBranchQuery(CreateBranchQueryRequest request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUpdateBranchQuery", Global.ConnectionString().ToString()
                                                            ,_parameterManager.Get(BranchQueryId, request.BranchQueryId)
                                                            , _parameterManager.Get(EdupreneurName,request.EdupreneurName)
                                                            , _parameterManager.Get(EdupreneurAddress, request.EdupreneurAddress)             
                                                            , _parameterManager.Get(Phone, request.Phone)                                     
                                                            , _parameterManager.Get(Email, request.Email) 
                                                            , _parameterManager.Get(BranchTypeId,request.BranchTypeId)
                                                            , _parameterManager.Get("@StateId", request.StateId)
                                                            , _parameterManager.Get("@DistrictId", request.DistrictId)
                                                            , _parameterManager.Get(PostOfficeName, request.PostOfficeName)
                                                            , _parameterManager.Get(PoliceStationName, request.PoliceStationName)
                                                            , _parameterManager.Get(Location, request.Location)
                                                            , _parameterManager.Get(PinCode, request.PinCode)
                                                            , _parameterManager.Get(Comment, request.PinCode)
                                                            , _parameterManager.Get(UpdatedBy, 0)//need to update here 
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }   
        public DbDataReader UpdateIsReadBranchQuery(int branchQueryID ,bool isRead)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_UpdateBranchQueryRead", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(BranchQueryId, branchQueryID)
                                                            , _parameterManager.Get(IsRead, isRead)
                                                            , _parameterManager.Get(UpdatedBy, null)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader InsertUpdateBranchOwner(string mxlStr)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUpdateBranchOwnerDetails", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.mxlStr, mxlStr)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader GetBranchType()
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_GetBranchType", Global.ConnectionString().ToString());

            }
            catch (Exception ex) { throw ex; }
        }
        public DbDataReader GetAllState()
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_GetAllStates", Global.ConnectionString().ToString());

            }
            catch (Exception ex) { throw ex; }
        }
        public DbDataReader GetAllDistrict(int? StateId = null)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_GetDistricts", Global.ConnectionString().ToString(),
                     _parameterManager.Get(StateCode, StateId.HasValue ? StateId.Value.ToString() : null)

                    );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader GetBranchUsers(int? BranchID = null)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_GetBranchUsers", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.BranchID, BranchID)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }

    }
}
