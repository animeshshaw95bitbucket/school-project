﻿using System;
using System.Data.Common;
using WebAPI.Core.Code;
using WebAPI.Core.RequestModel;
using WebAPI.Data.Code;
using WebAPI.Data.DBContext;


namespace WebAPI.Data.DBAccess
{
   // [Athorize]
    public class HeadOfficeDBAccessModel
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly IGlobal Global;
        private readonly SqlServerParamManager _parameterManager;
        // private readonly JSONConverter _JSONConverter;
        #endregion

        #region CTOR
        public HeadOfficeDBAccessModel(IGlobal global)
        {
            Global = global;
            DB = new DataConnection();
            _parameterManager = new SqlServerParamManager();
            //  _JSONConverter = new JSONConverter();
        }
        #endregion

        #region Constants
        private readonly String HeadOfficeID = "@HeadOfficeID";
        private readonly String Name = "@Name";
        private readonly String Email = "@Email";
        private readonly String Password = "@Password";
        private readonly String Phone = "@Phone";
        private readonly String IsAdmin = "@IsAdmin";
        private readonly String IsActive = "@IsActive";
        private readonly String IsDelete = "@IsDelete";





        #endregion


        /// <summary>
        /// Get Head Office Master
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DbDataReader GetHeadOfficeMasters(HeadOfficeGetRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_GetHeadOffice", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(HeadOfficeID, request.Id)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }


        /// <summary>
        /// Insert HeadOffice
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DbDataReader InsertUpdateHeadOffice(HeadOfficeCreateRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUpdateHeadOfficeMaster", Global.ConnectionString().ToString(),
                                                           _parameterManager.Get(HeadOfficeID, request.HeadOfficeID),
                                                           _parameterManager.Get(Name, request.Name),
                                                           _parameterManager.Get(Email, request.Email),
                                                           _parameterManager.Get(Phone, request.Phone),
                                                           _parameterManager.Get(Password, request.Password),
                                                           _parameterManager.Get(IsAdmin, request.IsAdmin),
                                                           _parameterManager.Get(IsActive, request.IsActive),
                                                           _parameterManager.Get(IsDelete, request.IsDelete)

                                                          );
            }
            catch (Exception ex) { throw ex; }
        }


        ///// <summary>
        ///// Update Client
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //public DbDataReader UpdateClient(ClientUpdateRequestModel request)
        //{
        //    try
        //    {
        //        return DB.ExecuteStoredProcedureForReader(StoredProcedures.usp_Client_Update, Global.ConnectionString().ToString(),
        //                                                   _parameterManager.Get(ClientID, request.ClientID),
        //                                                   _parameterManager.Get(ClientCode, request.ClientCode),
        //                                                   _parameterManager.Get(ClientName, request.ClientName),
        //                                                   _parameterManager.Get(ParentID, request.ParentID),
        //                                                   _parameterManager.Get(FirstName, request.FirstName),
        //                                                   _parameterManager.Get(LastName, request.LastName),
        //                                                   _parameterManager.Get(ClientNumber, request.ClientNumber),
        //                                                   _parameterManager.Get(ClientSite, request.ClientSite),
        //                                                   _parameterManager.Get(ClientTypeID, request.ClientTypeID),
        //                                                   _parameterManager.Get(IndustryID, request.IndustryID),
        //                                                   _parameterManager.Get(AnnualRevenue, request.AnnualRevenue),
        //                                                   _parameterManager.Get(RatingID, request.RatingID),
        //                                                   _parameterManager.Get(Phone, request.Phone),
        //                                                   _parameterManager.Get(Mobile, request.Mobile),
        //                                                   _parameterManager.Get(Fax, request.Fax),
        //                                                   _parameterManager.Get(Email, request.Email),
        //                                                   _parameterManager.Get(Website, request.Website),
        //                                                   _parameterManager.Get(OwnershipID, request.OwnershipID),
        //                                                   _parameterManager.Get(Employees, request.Employees),
        //                                                   _parameterManager.Get(SICCode, request.SICCode),
        //                                                   _parameterManager.Get(Street, request.Street),
        //                                                   _parameterManager.Get(City, request.City),
        //                                                   _parameterManager.Get(StateID, request.StateID),
        //                                                   _parameterManager.Get(Zip, request.Zip),
        //                                                   _parameterManager.Get(Country, request.Country),
        //                                                   _parameterManager.Get(Description, request.Description),
        //                                                   _parameterManager.Get(ClientOwner, _JSONConverter.ToJSON(request.objOwner)),
        //                                                   _parameterManager.Get(AddedBy, request.UserId),
        //                                                   _parameterManager.Get(IsSaveOldClientName, request.IsSaveOldClientName),
        //                                                   _parameterManager.Get(FileURL, request.FileURL)
        //                                                  );
        //    }
        //    catch (Exception ex) { throw ex; }
        //}


        ///// <summary>
        ///// Delete Client
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //public DbDataReader DeleteClient(ClientDeleteRequestModel request)
        //{
        //    try
        //    {
        //        return DB.ExecuteStoredProcedureForReader(StoredProcedures.usp_Client_Delete, Global.ConnectionString().ToString(),
        //                                               _parameterManager.Get(ClientID, request.ClientID),
        //                                               _parameterManager.Get(AddedBy, request.UserId)
        //                                              );
        //    }
        //    catch (Exception ex) { throw ex; }
        //}


        ///// <summary>
        ///// Update Active InActive for Client
        ///// </summary>
        ///// <param name="clientID"></param>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //public DbDataReader UpdateActiveInActiveClient(Int64 clientID, ClientActiveInActiveRequestModel request)
        //{
        //    try
        //    {
        //        return DB.ExecuteStoredProcedureForReader(StoredProcedures.usp_Client_UpdateActiveInActive, Global.ConnectionString().ToString(),
        //                                               _parameterManager.Get(ClientID, clientID),
        //                                               _parameterManager.Get(Active, request.IsActive),
        //                                               _parameterManager.Get(AddedBy, request.UserId)
        //                                              );
        //    }
        //    catch (Exception ex) { throw ex; }
        //}

        ///// <summary>
        ///// Get Auto Code
        ///// </summary>
        ///// <returns></returns>
        //public DbDataReader GetAutoCode()
        //{
        //    try
        //    {
        //        return DB.ExecuteStoredProcedureForReader(StoredProcedures.usp_Client_GetAutoCode, Global.ConnectionString().ToString());
        //    }
        //    catch (Exception ex) { throw ex; }
        //}


    }
}
