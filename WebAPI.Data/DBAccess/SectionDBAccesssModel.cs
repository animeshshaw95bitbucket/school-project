﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.DBAccess
{
    public class SectionDBAccesssModel
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly IGlobal Global;
        private readonly SqlServerParamManager _parameterManager;
        // private readonly JSONConverter _JSONConverter;
        #endregion

        #region CTOR
        public SectionDBAccesssModel(IGlobal global)
        {
            Global = global;
            DB = new DataConnection();
            _parameterManager = new SqlServerParamManager();
        }
        #endregion

        #region Constents
        private readonly String SectionID = "@SectionID";
        private readonly String ClassID = "@ClassID";
        private readonly String SectionName = "@SectionName";
        private readonly String SectionDescription = "@SectionDescription";
        private readonly String IsActive = "@IsActive";
        private readonly String CreatedBy = "@CreatedBy";
        #endregion

        #region Public functions
        public DataSet GetSectionDetails(String sectionName = null, int? sectionID = null, string sectionCode = null)
        {
            try
            {
                List<PArray> _params = new List<PArray>()
                {
                    new PArray()
                    {
                        PName=SectionID,
                        PType=SqlDbType.Int,
                        PValue=sectionID ?? SqlInt32.Null
                    },
                    new PArray()
                    {
                        PName=SectionName,
                        PType=SqlDbType.NVarChar,
                        PValue=sectionName ?? SqlString.Null
                    }
                };
                return DB.getData("usp_GetSectionDetails", _params
                                    , Global.ConnectionString().ToString()
                                );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader InsertUpdateSection(SectionRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUpdateSection", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.SectionID, request.SectionID)
                                                            , _parameterManager.Get(this.SectionName, request.SectionName)
                                                            , _parameterManager.Get(this.SectionDescription, request.SectionDesc)
                                                            , _parameterManager.Get(this.ClassID, request.ClassID)
                                                            , _parameterManager.Get(this.IsActive, request.IsActive)
                                                            , _parameterManager.Get(this.CreatedBy, request.CreatedBy)

                                                            );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader UpdateStatusSection(StatusRequestModel request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_UpdateStatusSection", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.SectionID, request.ID)
                                                            , _parameterManager.Get(this.IsActive, request.IsActive)
                                                            , _parameterManager.Get(this.CreatedBy, request.CreatedBy)

                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
