﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.DBAccess
{
    public class StudentDBAccessModel
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly IGlobal Global;
        private readonly SqlServerParamManager _parameterManager;
        #endregion

        #region CTOR
        public StudentDBAccessModel(IGlobal global)
        {
            Global = global;
            DB = new DataConnection();
            _parameterManager = new SqlServerParamManager();
        }
        #endregion

        #region Constants
        private readonly String StudentQueryId = "@StudentQueryID";
        private readonly String StudentName = "@StudentName";
        private readonly String FathersName = "@FathersName";
        private readonly String BranchID = "@BranchID";
        private readonly String BranchName = "@BranchName";
        private readonly String StateId = "@StateId";
        private readonly String DistrictId = "@DistrictId";
        private readonly String UserID = "@UserID";
        private readonly String IsRead = "@IsRead";
        private readonly String Phone = "@Phone";
        private readonly String Email = "@Email";
        private readonly String Location = "@Location";
        private readonly String PinCode = "@PinCode";
        private readonly String Query = "@Query";
        private readonly String Comment = "@Comment";
        private readonly String IsRespond = "@IsRespond";
        private readonly String Status = "@Status";
        private readonly String Gender = "@Gender";
        private readonly String SearchText = "@SearchText";
        #endregion

        #region Student Query
        public DbDataReader AddUpdateStudentQuery(AddUpdateStudentQueryRequest request)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUpdateStudentQuery", Global.ConnectionString().ToString()
                                                            , _parameterManager.Get(StudentQueryId, request.StudentQueryID)
                                                            , _parameterManager.Get(StudentName, request.StudentName)
                                                            , _parameterManager.Get(BranchID, request.BranchId)
                                                            , _parameterManager.Get(Phone, request.Phone)
                                                            , _parameterManager.Get(Email, request.Email)
                                                            , _parameterManager.Get(Gender, request.Gender)
                                                            , _parameterManager.Get(FathersName, request.FathersName)
                                                            , _parameterManager.Get(Query, request.Query)
                                                            , _parameterManager.Get(Comment, request.Comment)
                                                            , _parameterManager.Get(StateId, request.StateId)
                                                            , _parameterManager.Get(DistrictId, request.DistrictId)
                                                            , _parameterManager.Get(Location, request.Location)
                                                            , _parameterManager.Get(PinCode, request.PinCode)
                                                            , _parameterManager.Get(UserID, request.UserId)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        public DbDataReader UpdateIsReadStudentQuery(int StudentQueryID, bool isRead)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_UpdateStudentQueryRead", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(StudentQueryId, StudentQueryID)
                                                            , _parameterManager.Get(IsRead, isRead)
                                                            , _parameterManager.Get(UserID, null)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet GetStudentQuery(SearchStudentQueryRequest request)
        {
            request = request ?? new SearchStudentQueryRequest();
            try
            {
                return DB.ExecuteStoredProcedureForDataset("usp_GetStudentQuery", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(StudentQueryId, request.StudentQueryID)
                                                            , _parameterManager.Get(BranchID, request.BranchId)
                                                            , _parameterManager.Get(BranchName, request.BranchName)
                                                            , _parameterManager.Get(StudentName, request.StudentName)
                                                            , _parameterManager.Get(IsRead, request.IsRead)
                                                            , _parameterManager.Get(IsRespond, request.IsRespond)
                                                            , _parameterManager.Get(SearchText, request.SearchText)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
