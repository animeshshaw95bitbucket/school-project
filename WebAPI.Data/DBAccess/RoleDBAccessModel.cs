﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.DBAccess
{
    public sealed class RoleDBAccessModel
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly IGlobal Global;
        private readonly SqlServerParamManager _parameterManager;
        #endregion

        #region CTOR
        public RoleDBAccessModel(IGlobal global)
        {
            Global = global;
            DB = new DataConnection();
            _parameterManager = new SqlServerParamManager();
        }
        #endregion
        #region Constants
        private readonly String UserID = "@UserID";
        #endregion

        #region Public Methods
        public DbDataReader GetUserRoles(int UserID)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_GetUserRoles", Global.ConnectionString().ToString(),
                                                            _parameterManager.Get(this.UserID, UserID)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        public DbDataReader GetAllRoles()
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_GetAllRole", Global.ConnectionString().ToString());

            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
