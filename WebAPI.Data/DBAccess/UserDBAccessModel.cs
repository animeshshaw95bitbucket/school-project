﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.Code;
using WebAPI.Core.RequestModel;
using WebAPI.Data.DBContext;

namespace WebAPI.Data.DBAccess
{
    public sealed class UserDBAccessModel
    {
        #region Global Variable
        private readonly DataConnection DB;
        private readonly IGlobal _global;
        private readonly SqlServerParamManager _parameterManager;
        #endregion

        #region CTOR
        public UserDBAccessModel(IGlobal global)
        {
            _global = global;
            DB = new DataConnection();
            _parameterManager = new SqlServerParamManager();
        }
        #endregion
        #region Constants
        private readonly String UserID = "@UserID";
        private readonly String RoleIds = "@RoleIds";
        private readonly String FirstName = "@FirstName";
        private readonly String LastName = "@LastName";
        private readonly String EmailID = "@EmailID";
        private readonly String Password = "@Password";
        private readonly String IsActive = "@IsActive";
        private readonly String PhoneNumber = "@PhoneNumber";
        private readonly String BranchTypeID = "@BranchTypeID";
        private readonly String BranchID = "@BranchID";
        private readonly String UserName = "@UserName";
        #endregion

        #region Public Methods
        public DbDataReader AddUser(AddUserRequestModel model)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUser", _global.ConnectionString().ToString(),
                                                            _parameterManager.Get(UserID, model.UserID),
                                                            _parameterManager.Get(FirstName, model.FirstName ?? SqlString.Null),
                                                            _parameterManager.Get(LastName, model.LastName ?? SqlString.Null),
                                                            _parameterManager.Get(EmailID, model.EmailID ?? SqlString.Null),
                                                            _parameterManager.Get(Password, model.Password ?? SqlString.Null),
                                                            _parameterManager.Get(IsActive, model.IsActive),
                                                            _parameterManager.Get(PhoneNumber, model.PhoneNumber??SqlString.Null)

                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        public DbDataReader UpdateUserPassword(AddUserRequestModel model)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_UpdateUserPassword", _global.ConnectionString().ToString(),
                                                           
                                                          _parameterManager.Get(EmailID, model.EmailID),
                                                            _parameterManager.Get(Password, model.Password)

                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        public DbDataReader GetUsersDetails(int? userID,int? branchID,string userName)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_GetUserDetails", _global.ConnectionString().ToString(),
                                                            _parameterManager.Get(UserID, userID),
                                                            _parameterManager.Get(BranchID, branchID),
                                                            _parameterManager.Get(UserName, userName)

                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        public DbDataReader GetUserByUserName(string userName)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_GetUserByEmail", _global.ConnectionString().ToString(),
                                                            _parameterManager.Get(EmailID, userName)

                                                            );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader InsertUpdteUserRole(int userID, string roleIds)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_InsertUpdateUserRoles", _global.ConnectionString().ToString(),
                                                            _parameterManager.Get(UserID, userID),
                                                            _parameterManager.Get(RoleIds, roleIds)

                                                            );

            }
            catch (Exception ex) { throw ex; }
        }

        public DbDataReader UpdateProfile(UpdateProfileRequestModel model)
        {
            try
            {
                return DB.ExecuteStoredProcedureForReader("usp_UpdateProfile", _global.ConnectionString().ToString(),
                                                            _parameterManager.Get(UserID, model.UserID),
                                                            _parameterManager.Get(FirstName, model.FirstName ?? SqlString.Null),
                                                            _parameterManager.Get(LastName, model.LastName ?? SqlString.Null),
                                                            _parameterManager.Get(EmailID, model.EmailID ?? SqlString.Null),
                                                            _parameterManager.Get(PhoneNumber, model.PhoneNumber ?? SqlString.Null)
                                                            );

            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
