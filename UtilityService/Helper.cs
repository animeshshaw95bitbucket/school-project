﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace UtilityService
{
    public static  class Helper
    {
        public static  bool SendVerifyEmail(string formMail, string toMail,string sub,string body,bool isHtml)
        {
            try
            {
                var smtp = new SmtpClient
                {
                    Host = "127.0.0.1",
                    UseDefaultCredentials = true
                };

                MailMessage mailMessage = new MailMessage(formMail, toMail,sub, body);
                mailMessage.IsBodyHtml = isHtml;
               
                smtp.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static string BuildResetPasswordEmailBody(string userName)
        {
            var url = ConfigurationManager.AppSettings["WebSiteURL"].ToString();
            string body;
            using (var wc = new System.Net.WebClient())
                body = wc.DownloadString(url +"content/ResetPassword.html");

            var maskString = userName + "," + DateTime.Now.AddHours(24).ToString();

            var resetUrl = url + "login/ResetPassword?str="+Convert.ToBase64String(Encoding.UTF8.GetBytes(maskString));
            body = body.Replace("{ResetUrl}", resetUrl); //replacing the required things  

            //body = body.Replace("{SecretKey}", secretKey);
            //body = body.Replace("{UnsubscribeUrl}", "https://chippercircle.com/unsubscribe?email=" + updateCommonFieldRequest.Email);

            return body;
        }
    }
}
