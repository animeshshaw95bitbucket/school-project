﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.RequestModel
{
    public class SectionRequestModel
    {
        public int SectionID { get; set; }
        public string SectionName { get; set; }
        public string SectionDesc { get; set; }
        public Int32 ClassID { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
    }
}
