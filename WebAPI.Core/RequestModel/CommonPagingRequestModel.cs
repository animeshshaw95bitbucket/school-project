﻿using System;
using System.ComponentModel;

namespace WebAPI.Core.RequestModel
{
    public class CommonPagingRequestModel
    {
        [DisplayName("IsActive")]
        public Int32? IsActive { get; set; }

        [DisplayName("SearchText")]
        public String SearchText { get; set; }

        [DisplayName("PageNumber")]
        public Int32? PageNumber { get; set; }

        [DisplayName("PageSize")]
        public Int32? PageSize { get; set; }

        [DisplayName("UsePaging")]
        public Boolean UsePaging { get; set; }

        [DisplayName("Sorting By")]
        public String SortingBy { get; set; }

        [DisplayName("SortingOrder")]
        public String SortingOrder { get; set; }

        [DisplayName("IsEmployeePortal")]
        public Boolean? IsEmployeePortal { get; set; }
    }
}
