﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.RequestModel
{
    public class AddUpdateStudentQueryRequest
    {
        public int StudentQueryID { get; set; }

        [Required]
        public int BranchId { get; set; }

        [Required]
        public String StudentName { get; set; }

        [Required]
        public String Phone { get; set; }

        [Required]
        public String Email { get; set; }

        [Required]
        public Int32 Gender { get; set; }

        public String FathersName { get; set; }
        public String Query { get; set; }
        public String Comment { get; set; }
        public String StateId { get; set; }
        public String DistrictId { get; set; }
        public String Location { get; set; }
        public String PinCode { get; set; }
        public Int32 UserId { get; set; }
    }

    public class SearchStudentQueryRequest
    {
        public Int32? StudentQueryID { get; set; }
        public Int32? BranchId { get; set; }
        public String StudentName { get; set; }
        public String BranchName { get; set; }
        public Boolean? IsRead { get; set; }
        public Boolean? IsRespond { get; set; }
        public Int32? Status { get; set; }
        public String SearchText { get; set; }
    }

    public class UpdateIsReadStudentQueryRequest
    {
        [Required]
        public Int32 StudentQueryID { get; set; }

        [Required]
        public Boolean IsRead { get; set; }
        public Int32? UserId { get; set; }
    }
}
