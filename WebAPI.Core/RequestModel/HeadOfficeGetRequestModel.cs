﻿using System;
using System.ComponentModel;

namespace WebAPI.Core.RequestModel
{
   public sealed class HeadOfficeGetRequestModel : CommonPagingRequestModel
    {
        public int Id { get; set; }
    }
}
