﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.RequestModel
{
   public class HeadOfficeCreateRequestModel : CommonRequestModel
    {
        public int HeadOfficeID { get; set; }
        public String Name { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public Boolean IsAdmin { get; set; }
    }
}
