﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.RequestModel
{
    public sealed class AuthenticateRequestModel
    {
        public String UserName { get; set; }
        public String Password { get; set; }

    }
}
