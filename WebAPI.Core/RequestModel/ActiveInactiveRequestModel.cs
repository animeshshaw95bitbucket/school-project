﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Core.RequestModel
{
    public sealed class ActiveInactiveRequestModel
    {
        [Required]
        [DisplayName("Is Active")]
        public Boolean IsActive { get; set; }

        [Required]
        [DisplayName("UserId")]
        public int UserId { get; set; } 
    }

    public sealed class ActiveInactiveNARequestModel
    {
        [Required]
        [DisplayName("Is Active")]
        public Boolean IsActive { get; set; }

        [DisplayName("UserId")]
        public int? UserId { get; set; }
    }
}
