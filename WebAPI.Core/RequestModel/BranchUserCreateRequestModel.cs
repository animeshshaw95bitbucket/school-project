﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.RequestModel
{
    public class BranchUserCreateRequestModel
    {
        [Required(ErrorMessage ="BranchID Required")]
        public int BranchID { get; set; }
        [Required(ErrorMessage = "UserID Required")]
        public int UserID { get; set; }
        public bool IsActive { get; set; }
    }
}
