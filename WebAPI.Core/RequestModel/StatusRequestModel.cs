﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.RequestModel
{
    public class StatusRequestModel
    {
        public int ID { get; set; }
        public bool IsActive { get; set; } = true;
        public int CreatedBy { get; set; }
    }
}
