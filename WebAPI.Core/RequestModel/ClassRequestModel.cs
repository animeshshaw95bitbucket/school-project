﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.RequestModel
{
    public class ClassRequestModel
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public string ClassDescription { get; set; }
        public int SessionID { get; set; }
        public bool IsActive { get; set; } = true;
        public int CreatedBy { get; set; }
    }
}
