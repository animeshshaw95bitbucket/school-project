﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Core.RequestModel
{
    public class CommonRequestModel
    {
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public DateTime UpdatedDate { get; set; } = DateTime.Now;
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }

    }
}
