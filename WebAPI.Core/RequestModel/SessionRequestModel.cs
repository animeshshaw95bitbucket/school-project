﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.RequestModel
{
    public class SessionRequestModel
    {
        public int SessionID { get; set; }
        public string SessionName { get; set; }
        public string SessionDesc { get; set; }
        public string PrincipalSignaturePath { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }

    }
}
