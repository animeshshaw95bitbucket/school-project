﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.RequestModel
{
    public class StudentCreateRequestModel
    {
        public long StudentID { get; set; }
        public string StudentCode { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public DateTime DOA { get; set; }
        public string BloodGroup { get; set; }
        public string Nationality { get; set; }
        public string AdmissionCode { get; set; }
        public string Caste { get; set; }
        public string SubCaste { get; set; }
        public string CertificateNumber { get; set; }
        public string CertificateIssueBox { get; set; }
        public string Religion { get; set; }
        public bool IsPhysicallyHandicap { get; set; }
        public int PhysicallyHandicapPercentage { get; set; }
        public string PhysicallyHandicapDescription { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public int Class { get; set; }
        public string Session { get; set; }
        public string StudentSign { get; set; }
        public string StudentPhoto { get; set; }
        public bool IsHostelAssign { get; set; }
        public bool IsVechicleAssign { get; set; }
    }
}
