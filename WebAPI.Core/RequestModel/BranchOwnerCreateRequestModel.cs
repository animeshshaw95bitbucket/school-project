﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.RequestModel
{
    public class BranchOwnerCreateRequestModel
    {
        public int BranchOwnerID { get; set; }
        public int BranchID { get; set; }
        public String OwnerName { get; set; }
        public String QualificationID { get; set; }
        public String Designation { get; set; }
        public DateTime DOB { get; set; }
        public DateTime DOJ { get; set; }
        public String AadharNo { get; set; }
        public String VoterNo { get; set; }
        public String PanNo { get; set; }
        public String PrimaryContactNo { get; set; }
        public String SecondaryContactNo { get; set; }
        public String EmailAddress { get; set; }
        public bool IsActive { get; set; }
    }
}