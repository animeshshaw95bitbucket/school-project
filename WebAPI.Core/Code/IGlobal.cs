﻿using System;

namespace WebAPI.Core.Code
{
    public interface IGlobal
    {
        /// <summary>
        /// ConnectionString
        /// </summary>
        /// <returns></returns>
        String ConnectionString();

        /// <summary>
        /// DefaultPageListSize
        /// </summary>
        /// <returns></returns>
        Int32 DefaultPageListSize();       

        /// <summary>
        /// Website Url
        /// </summary>
        /// <returns></returns>
        String WebsiteUrl();

        /// <summary>
        /// Website Name
        /// </summary>
        /// <returns></returns>
        String WebsiteName();

        /// <summary>
        /// File Upload Path
        /// </summary>
        /// <returns></returns>
        String FileUploadPath();

        /// <summary>
        /// FrontEndWebsiteURL
        /// </summary>
        /// <returns></returns>
        String FrontEndWebsiteURL();

        /// <summary>
        /// FileUploadRawPath
        /// </summary>
        /// <returns></returns>
        String FileUploadRawPath();

        /// <summary>
        /// IOS Cirtificate path
        /// </summary>
        /// <returns></returns>
        String IOSCert();

        /// <summary>
        /// Error Log
        /// </summary>
        /// <returns></returns>
        String Error();

        /// <summary>
        /// TempFolderPath
        /// </summary>
        /// <returns></returns>
        String TempFolderPath();

        /// <summary>
        /// FrontEndWebFolderPath
        /// </summary>
        /// <returns></returns>
        String FrontEndWebFolderPath();

        /// <summary>
        /// GeneralErrorMessgae
        /// </summary>
        /// <returns></returns>
        String GeneralErrorMessgae();
        /// <summary>
        /// AudienceID
        /// </summary>
        /// <returns></returns>
        String AudienceID();
        /// <summary>
        /// Issuer
        /// </summary>
        /// <returns></returns>
        String Issuer();
        /// <summary>
        /// Issuer
        /// </summary>
        /// <returns></returns>
        String SignKey();
        /// <summary>
        /// Auth Token Expiry
        /// </summary>
        /// <returns></returns>
        String AuthTokenExpiry();
    }
}
