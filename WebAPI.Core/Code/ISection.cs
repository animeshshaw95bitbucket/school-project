﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;

namespace WebAPI.Core.Code
{
    public interface ISection
    {
        IEnumerable<SectionDataModel> GetSection(int? sectionID = null, string sectionName = null, string sectionCode = null);
        CommonResponseDataModel InsertUpdateSection(SectionRequestModel request);
        CommonResponseDataModel UpdateStatusSection(StatusRequestModel request);
    }
}
