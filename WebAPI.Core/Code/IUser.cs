﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;

namespace WebAPI.Core.Code
{
   public interface IUser
    {
        CommonResponseDataModel AddUser(AddUserRequestModel model);
        CommonResponseDataModel UpdateUserPassword(AddUserRequestModel model);
        UserDetailsResponseModel GetUsersDetails(int? UserID = null, int? BranchId = null, string BranchName = null);
        UserDetailsResponseModel GetUserByUserName(string userName);
        CommonResponseDataModel InsertUpdteUserRole(int UserID,string RoleIds);
        UserDataModel GetUserProfile(int? UserID = null,string UserName = null);
        CommonResponseDataModel UpdateProfile(UpdateProfileRequestModel model);
    }
}
