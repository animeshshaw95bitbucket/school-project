﻿using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.Code
{
    public interface IAuthenticate
    {
        AuthenticateDataModels UserLogin(AuthenticateRequestModel requestModel);
    }
}
