﻿using System;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;
using System.Collections.Generic;
namespace WebAPI.Core.Code
{
    public interface IHeadOfficeMaster
    {

        /// <summary>
        /// Get HeadOffice
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        IEnumerable<HeadOfficeDataModel> GetHeadOffice(HeadOfficeGetRequestModel requestModel);


        /// <summary>
        /// Insert HeadOffice
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        CommonResponseDataModel InsertHeadOfficeMaster(HeadOfficeCreateRequestModel requestData);


        ///// <summary>
        ///// Update Client
        ///// </summary>
        ///// <param name="requestData"></param>
        ///// <returns></returns>
        //CommonResponseDataModel UpdateClient(ClientUpdateRequestModel requestData);


        ///// <summary>
        ///// Delete Client
        ///// </summary>
        ///// <param name="requestData"></param>
        ///// <returns></returns>
        //CommonResponseDataModel DeleteClient(ClientDeleteRequestModel requestData);


        ///// <summary>
        ///// Active in Active Update Client
        ///// </summary>
        ///// <param name="clientID"></param>
        ///// <param name="requestData"></param>
        ///// <returns></returns>
        //CommonResponseDataModel UpdateActiveinActiveClient(Int64 clientID, ClientActiveInActiveRequestModel requestData);


    }
}
