﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;

namespace WebAPI.Core.Code
{
    public interface IBranch
    {
        IEnumerable<BranchDetails> GetBranchDetails(String branchName = null,int ? BranchID = null, int? BranchOwnerID = null, int? BranchTypeID = null, int? StateID = null, int? DistrictID = null);
        IEnumerable<BranchMaster> GetBranchListDDL();
        CommonResponseDataModel InsertUpdateBranch(BranchCreateRequestModel request);
        CommonResponseDataModel InsertUpdateBranchOwner(List<BranchOwnerCreateRequestModel> request);
        IEnumerable<BranchTypes> GetBranchTypes();
        IEnumerable<StateList> GetAllState();
        IEnumerable<DistrictDataModel> GetAllDistrict(int? stateId=null);
        CommonResponseDataModel InsertUpdateBranchUser(BranchUserCreateRequestModel request);
        CommonResponseDataModel InsertUpdateBranchQuery(CreateBranchQueryRequest request);
        CommonResponseDataModel UpdateIsReadBranchQuery(int BranchQueryID,bool isRead );
        BranchUsersDetails  GetBranchUsers(int? BranchID);
        IEnumerable<BranchQueryResponseDataModel> GetBranchQueryDetails(int? branchQueryID);

    }
}
