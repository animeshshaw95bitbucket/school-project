﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;

namespace WebAPI.Core.Code
{
    public interface IClass
    {
        IEnumerable<ClassDataModel> GetClass(int? classID = null, string className = null);

        CommonResponseDataModel InsertUpdateClass(ClassRequestModel request);

        CommonResponseDataModel UpdateStatusClass(StatusRequestModel request);
    }
}
