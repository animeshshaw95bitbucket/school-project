﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.Code
{
    public interface IRefreshToken
    {
        void Save(string refreshToken, string userName, Double expiry);
        void RemoveRefreshToken(string refreshToken, string userName);
        bool HasValidRefreshToken(string refreshToken, string userName);
    }
}
