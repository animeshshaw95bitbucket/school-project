﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.DataModel;

namespace WebAPI.Core.Code
{
    public interface IRole
    {
        IEnumerable<RoleDataModel> GetAllRoles();
        IEnumerable<RoleDataModel> GetUserRoles(int UserID);
    }
}
