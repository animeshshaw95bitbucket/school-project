﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;

namespace WebAPI.Core.Code
{
    public interface ISession
    {
        IEnumerable<SessionMaster> GetSession(int? sessionID = null, string sessionName = null, string classCode = null);
        CommonResponseDataModel InsertUpdateSession(SessionRequestModel request);
        CommonResponseDataModel UpdateStatusSession(StatusRequestModel request);
    }
}
