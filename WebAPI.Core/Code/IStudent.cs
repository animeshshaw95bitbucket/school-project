﻿using System;
using System.Collections.Generic;
using WebAPI.Core.DataModel;
using WebAPI.Core.RequestModel;

namespace WebAPI.Core.Code
{
    public interface IStudent
    {
        #region Student Query
        CommonResponseDataModel AddUpdateStudentQuery(AddUpdateStudentQueryRequest request);
        CommonResponseDataModel UpdateIsReadStudentQuery(int StudentQueryID, bool isRead);
        IEnumerable<StudentQueryResponseDataModel> GetStudentQuery(SearchStudentQueryRequest request);
        #endregion

    }
}
