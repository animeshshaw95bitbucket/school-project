﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.DataModel
{
  public  class BranchTypes
    {
        public int BranchTypeId { get; set; }

        public string BranchTypeName { get; set; }
    }
}
