﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.DataModel
{
    public class RoleDataModel
    {
        public Int32 RoleID { get; set; }
        public String RoleName { get; set; }
        public Boolean IsActive { get; set; }
        
    }
}
