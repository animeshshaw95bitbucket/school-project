﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.DataModel
{
    public class UserDataModel : CommonResponseDataModel
    {
        public int UserID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String EmailID { get; set; }
        public String Password { get; set; }
        public String PhoneNumber { get; set; }
        public Nullable<int> BranchID { get; set; }
    }
}
