﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.DataModel
{
    public class SessionMaster
    {
        public int SessionID { get; set; }
        public int SectionID { get; set; }
        public string SessionName { get; set; }
        public string SessionDescription { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string PrincipalSignature { get; set; }
    }
}
