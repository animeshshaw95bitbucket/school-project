﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.DataModel
{
   public class UserDetailsResponseModel
    {
        
      public IEnumerable<UserDataModel> Users { get; set; }
      public IEnumerable<RoleDataModel> Roles { get; set; }
      public IEnumerable<UserRoleMappingModel> UserRole { get; set; }
    }
}
