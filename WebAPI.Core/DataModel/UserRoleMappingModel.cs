﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.DataModel
{
   public class UserRoleMappingModel
    {
        public int UserID { get; set; }
        public int RoleID { get; set; }
    }
}
