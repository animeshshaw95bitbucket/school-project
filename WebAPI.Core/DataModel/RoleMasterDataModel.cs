﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.DataModel
{
   public class RoleMasterDataModel
    {
        public int RoleID { get; set; }
        public String RoleName { get; set; }
    }
}
