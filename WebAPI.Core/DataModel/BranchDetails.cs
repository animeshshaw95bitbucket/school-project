﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.DataModel
{
    public class BranchDetails
    {
        public BranchMaster Branch { get; set; }
        public List<BranchOwner> BranchOwners { get; set; }
       
    }
}
