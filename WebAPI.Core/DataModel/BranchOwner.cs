﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.DataModel
{
    public class BranchOwner
    {
        public int BranchOwnerID { get; set; }
        public int BranchID { get; set; }
        public string OwnerName { get; set; }
        public int QualificationID { get; set; }
        public string QualificationName { get; set; }
        public string Designation { get; set; }
        public DateTime DOB { get; set; }
        public DateTime DOJ { get; set; }
        public string AadharNo { get; set; }
        public string VoterNo { get; set; }
        public string PanNo { get; set; }
        public string PrimaryContactNo { get; set; }
        public string SecondaryContactNo { get; set; }
        public string EmailAddress { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
