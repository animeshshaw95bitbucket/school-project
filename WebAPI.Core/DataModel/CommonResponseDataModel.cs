﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Core.DataModel
{
   public class CommonResponseDataModel
    {        
        [DisplayName("Return ID")]
        public int ReturnID { get; set; }

       
        [DisplayName("Message")]
        public String Message { get; set; }

       
        [DisplayName("Is Successful")]
        public Boolean Successful { get; set; }

        public Int32 Code { get; set; }

        public String AutoCode { get; set; }

        public String CommonText { get; set; }

        [DisplayName("TotalRecord")]
        public Int32 TotalRecord { get; set; }

       // public IEnumerable<CommonPermissionResponseDataModel> Permission { get; set; }

        public Int32 SectionViewPermission { get; set; }

        public Boolean IsAuthorized { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Boolean IsActive { get; set; }
        public string ExtraFiled { get; set; }

    }
}
