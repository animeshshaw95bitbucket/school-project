﻿using System;

namespace WebAPI.Core.DataModel
{
    public class GenderResponseDataModel
    {
        public Int32 GenderID { get; set; }
        public String Gender { get; set; }
    }
}
